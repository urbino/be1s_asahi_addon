﻿using SAPbobsCOM;
using SAPbouiCOM.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace be1s_asahi_addon
{ 
        public static class CustomExtends
        {
            public static double toDouble(this string valor)
            {
                double val = 0;
                double.TryParse(valor, System.Globalization.NumberStyles.Number, System.Globalization.CultureInfo.InvariantCulture, out val);
                if (double.IsNaN(val))
                {
                    val = 0;
                }
                return val;
            }

            public static int ToSAP_RGB(this string hexaDecimalColorCode)
            {
                if (string.IsNullOrEmpty(hexaDecimalColorCode)) throw new Exception("Código Hexadecimal vazio.");
                if (hexaDecimalColorCode.Length != 7 || hexaDecimalColorCode[0] != '#')
                    throw new Exception("Código Hexadecimal inválido. Formato: (#000000)");
                System.Drawing.Color cor = System.Drawing.ColorTranslator.FromHtml(hexaDecimalColorCode);
                return cor.ToSAP_RGB();
            }

            public static int ToSAP_RGB(this System.Drawing.Color cor)
            {
                return cor.B * 65536 + cor.G * 256 + cor.R;
            }

            public static string ToSqlValue(this double val)
            {
                return val.ToString().Replace(",", ".");
            }

            public static string ToSqlValue(this float val)
            {
                return val.ToString().Replace(",", ".");
            }

            public static string ToSqlValue(this int val)
            {
                return val.ToString();
            }

            public static string ToSqlValue(this DateTime val)
            {
                return val.ToString("yyyyMMdd");
            }

            public static int toInt(this string valor)
            {
                int val = 0;
                int.TryParse(valor, out val);
                return val;
            }

            public static int toIntOrMinusOne(this string valor)
            {
                int val = -1;
                int.TryParse(valor, out val);
                return val; 
            }

            public static string Left(this string str, int Length)
            {
                if (str.Length <= Length)
                    return str;
                else
                    return str.Substring(0, Length);
            }

            public static string Rigth(this string str, int Length)
            {
                if (str.Length <= Length)
                    return str;
                else
                    return str.Substring(str.Length - 1 - Length - 1, Length);
            }

            public static bool IsNot_NullOrEmpty(this string str)
            {
                return !string.IsNullOrEmpty(str);
            }

            public static bool IsNullOrEmpty(this string str)
            {
                return string.IsNullOrEmpty(str);
            }

            public static MemoryDataTable toMemoryDatatable(this SAPbouiCOM.DataTable dt)
            {
                string xml = dt.SerializeAsXML(SAPbouiCOM.BoDataTableXmlSelect.dxs_All);
                return xml.XmlDeserializeFromString<MemoryDataTable>();
            }

            public static string XmlSerializeToString(this object objectInstance)
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(objectInstance.GetType());
                var sb = new StringBuilder();

                using (System.IO.TextWriter writer = new System.IO.StringWriter(sb))
                {
                    serializer.Serialize(writer, objectInstance);
                }

                return sb.ToString();
            }

            public static T XmlDeserializeFromString<T>(this string objectData)
            {
                return (T)XmlDeserializeFromString(objectData, typeof(T));
            }

            public static object XmlDeserializeFromString(this string objectData, Type type)
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(type);
                object result;

                using (System.IO.TextReader reader = new System.IO.StringReader(objectData))
                {
                    result = serializer.Deserialize(reader);
                }

                return result;
            } 

            public static object dt_get_value(this SAPbouiCOM.DataTable dt, string column, int row)
            {
                try
                {
                    return dt.GetValue(column, row);
                }
                catch (Exception ex)
                {
                    throw new Exception("Cloud not get value for: " + column + ", " + ex.Message + ".");
                }
            }

            public static string dt_get_string(this SAPbouiCOM.DataTable dt, string column, int row)
            {
                return (string)dt.GetValue(column, row);
            }

            public static void dt_set_value( this SAPbouiCOM.DataTable dt, string column, int row, object value)
            {
                try
                {
                    dt.SetValue(column, row, value);
                }
                catch (Exception ex)
                {
                    throw new Exception("Cloud not set value for: " + column + ", " + ex.Message + "," + value.ToString());
                }
            }

            public static void setUIValue(this SAPbouiCOM.Matrix oMatrix, int visRow, string column, string value)
            {
                try
                {
                    SAPbouiCOM.Column col = oMatrix.Columns.Item(column);
                    if (col.Type == SAPbouiCOM.BoFormItemTypes.it_EDIT || col.Type == SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
                    {
                        SAPbouiCOM.EditText e = (SAPbouiCOM.EditText)col.Cells.Item(visRow).Specific;
                        if (e.Value != value) e.Value = value; 
                    }
                    else if (col.Type == SAPbouiCOM.BoFormItemTypes.it_PICTURE)
                    {
                        SAPbouiCOM.PictureBox e = (SAPbouiCOM.PictureBox)col.Cells.Item(visRow).Specific;
                        if (e.Picture != value) e.Picture = value;
                    }
                    else if (col.Type == SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
                    {
                        SAPbouiCOM.ComboBox e = (SAPbouiCOM.ComboBox)col.Cells.Item(visRow).Specific;
                        if (e.Value != value) e.Select(value,SAPbouiCOM.BoSearchKey.psk_ByValue);
                    }
                    else if (col.Type == SAPbouiCOM.BoFormItemTypes.it_CHECK_BOX)
                    {
                        SAPbouiCOM.CheckBox e = (SAPbouiCOM.CheckBox)col.Cells.Item(visRow).Specific;
                        bool v = false;
                        if (value == "Y") v = true;
                        if (e.Checked != v) e.Checked = v;
                    }
                    else
                        throw new Exception(col.Type.ToString() + " not expected.");
                }
                catch (Exception ex)
                {
                    Application.SBO_Application.StatusBar.SetText("Cloud not set value for: " + column + ", " + ex.Message);
                }
            }

            public static void setFocus(this SAPbouiCOM.Matrix oMatrix, int visRow, string column)
            {
                try
                {
                    SAPbouiCOM.Column col = oMatrix.Columns.Item(column);
                    col.Cells.Item(visRow).Click(); 
                }
                catch (Exception ex)
                {
                    Application.SBO_Application.StatusBar.SetText("Cloud not set focus for: " + column + ", " + ex.Message);
                }
            }

            public static int getNewLineVisOrder(this SAPbouiCOM.Matrix oMatrix, string testColumnIDForValue)
            {
                int lastRow =0;
                try
                { 
                      lastRow = oMatrix.RowCount-1;
                      if (oMatrix.getString(lastRow, testColumnIDForValue) != "")
                      {
                            oMatrix.AddRow();
                            lastRow = oMatrix.RowCount - 1;
                      }  
                }
                catch (Exception ex)
                {
                    Application.SBO_Application.StatusBar.SetText("Cloud not return new line: " + ex.Message);
                }
                return lastRow;
            }


            public static string getString(this SAPbouiCOM.Matrix oMatrix, int visRow, string column)
            {
                try
                {
                    SAPbouiCOM.Column col = oMatrix.Columns.Item(column);
                    if (col.Type == SAPbouiCOM.BoFormItemTypes.it_EDIT || col.Type == SAPbouiCOM.BoFormItemTypes.it_LINKED_BUTTON)
                    {
                        SAPbouiCOM.EditText e = (SAPbouiCOM.EditText)col.Cells.Item(visRow).Specific;
                        return e.Value;
                    }
                    else if (col.Type == SAPbouiCOM.BoFormItemTypes.it_PICTURE)
                    {
                        SAPbouiCOM.PictureBox e = (SAPbouiCOM.PictureBox)col.Cells.Item(visRow).Specific;
                        return e.Picture;
                    }
                    else if (col.Type == SAPbouiCOM.BoFormItemTypes.it_COMBO_BOX)
                    {
                        SAPbouiCOM.ComboBox e = (SAPbouiCOM.ComboBox)col.Cells.Item(visRow).Specific;
                        return e.Value;
                    }
                    else
                        throw new Exception(col.Type.ToString() + " not expected.");
                }
                catch (Exception ex)
                {
                    Application.SBO_Application.StatusBar.SetText("Cloud not get value for: " + column + ", " + ex.Message);
                    return "";
                }
            }


            public static Matrix getMatrixData(this SAPbouiCOM.Matrix oMatrix)
            {
                // obtain matrix data for fast access in memory 
                string xml = oMatrix.SerializeAsXML(SAPbouiCOM.BoMatrixXmlSelect.mxs_All);
                Matrix objMatrix;
                System.Xml.Serialization.XmlSerializer mySerializer = new System.Xml.Serialization.XmlSerializer(typeof(Matrix));
                using (System.IO.MemoryStream myStream = new System.IO.MemoryStream())
                {
                    System.IO.StreamWriter writer = new System.IO.StreamWriter(myStream);
                    writer.Write(xml);
                    writer.Flush();
                    myStream.Position = 0;

                    objMatrix = (Matrix)mySerializer.Deserialize(myStream);
                };

                /*
                 //Build a string to view data that can be copied into excel
                  string mm = "";
                  foreach (var col in objMatrix.ColumnsInfo)
                  {
                      mm += col.Title + "\t";
                  }
                  mm += "\n";
                  foreach (var col in objMatrix.ColumnsInfo)
                  {
                      mm += col.UniqueID + "\t";
                  }                 
                  foreach (var row in objMatrix.Rows)
                  {
                       mm += "\n";               
                      foreach (var col in row.Columns)
                      {
                          mm += "\"" + col.Value + "\"\t";
                      }
                  };
                  */
                return objMatrix;
            }
        
        }
     
} 
