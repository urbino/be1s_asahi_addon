﻿using SAPbouiCOM.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace be1s_asahi_addon
{
    internal class Returnables
    {
        internal static bool addORupdateInUI(bool manualUserRequest)
        {
            var added = false;
            SAPbouiCOM.Form frmContext = SAPbouiCOM.Framework.Application.SBO_Application.Forms.ActiveForm;
            try
            {
                // If the focus is on UDFs window, find main window.
                if (frmContext.TypeEx.Substring(0, 1) == "-")
                {
                    string udfFormId = frmContext.UniqueID;
                    int formType = frmContext.TypeEx.Substring(1).toInt();
                    int formTCount = frmContext.TypeCount;
                    frmContext = SAPbouiCOM.Framework.Application.SBO_Application.Forms.GetFormByTypeAndCount(formType, formTCount);
                    if (frmContext.UDFFormUID != udfFormId)
                    {
                        throw new Exception("Not able to find the main doc window.");
                    }
                }
                frmContext.Freeze(true);

                //GetTable Name from DocNum bindings
                var dtsName = ((SAPbouiCOM.EditText)frmContext.Items.Item("8").Specific).DataBind.TableName;
                var T0 = frmContext.DataSources.DBDataSources.Item(dtsName);

                if (manualUserRequest == false
                    && (frmContext.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE || frmContext.Mode == SAPbouiCOM.BoFormMode.fm_UPDATE_MODE) == false) return false;

                bool autoAddReturnables = T0.GetValue("U_ASH_AutoReturnables", 0) == "Y";
                if (manualUserRequest == false
                   && autoAddReturnables == false) return false;

                SAPbouiCOM.Matrix item38_LinesMatrix = (SAPbouiCOM.Matrix)frmContext.Items.Item("38").Specific;
                var objMatrix = item38_LinesMatrix.getMatrixData(); //Get matrix data (fast processing way)

                foreach (var row in objMatrix.Rows)
                {
                    int visOrder = row.Columns.First<MatrixRowColumn>(col => col.ID == "0").Value.toInt();
                    var itemCode = row.Columns.First<MatrixRowColumn>(col => col.ID == "1").Value;
                    var whsCode = row.Columns.First<MatrixRowColumn>(col => col.ID == "24").Value;
                    var withouWtyPosting = row.Columns.First<MatrixRowColumn>(col => col.ID == "1250002121").Value;

                    if (withouWtyPosting == "Y" || itemCode == "") continue;//Do nothing fo this line

                    double qty = Helpers.SAP.ToDouble(row.Columns.First<MatrixRowColumn>(col => col.ID == "11").Value);
                    var U_ASC_RTN_ADDED = row.Columns.First<MatrixRowColumn>(col => col.ID == "U_ASC_RTN_ADDED").Value;
                    var U_ASC_RTN_ITEM = row.Columns.First<MatrixRowColumn>(col => col.ID == "U_ASC_RTN_ITEM").Value;
                    // if (U_ASC_RTN_ADDED == "Y") continue;//Skip if already has returnables assigned for curent line

                    // Check Allocations to suggest
                    var oRs = (SAPbobsCOM.Recordset)Helpers.SAP.DICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    var cri = @"CALL ASH_GET_RETURNABLES(
	                            ITEMCODE =>  '" + itemCode + @"' /*<NVARCHAR(30)>*/,
	                            QUANTITY => " + qty.ToSqlValue() + @" /*<DECIMAL>*/,
	                            RET => ?
                            );";
                    oRs.DoQuery(cri);
                    if (!oRs.EoF)
                    {
                        if (U_ASC_RTN_ADDED != "Y") item38_LinesMatrix.SetCellWithoutValidation(visOrder, "U_ASC_RTN_ADDED", "Y");
                        if (U_ASC_RTN_ITEM != itemCode) item38_LinesMatrix.SetCellWithoutValidation(visOrder, "U_ASC_RTN_ITEM", itemCode);

                        while (!oRs.EoF)
                        {
                            string emptyItemCode = (string)oRs.Fields.Item("ITEMCODE").Value;
                            string baseItemCode = (string)oRs.Fields.Item("BASEITEM").Value;
                            double emptyQty = (double)oRs.Fields.Item("QUANTITY").Value;

                            if (emptyQty == 0)
                            {
                                Application.SBO_Application.MessageBox("Quantity for returnable " + emptyItemCode + " could not be calculated. Please check masterdata definitions.");
                                continue;
                            }


                            //Find existing returnable line
                            int emptyVisOrder = 0;
                            double emptyOldQty = 0;
                            string emptyWhsCode = "";
                            foreach (var r in objMatrix.Rows)
                            {
                                var lnItem = r.Columns.First<MatrixRowColumn>(col => col.ID == "1").Value;
                                var lnBaseItem = r.Columns.First<MatrixRowColumn>(col => col.ID == "U_ASC_RTN_ITEM").Value;

                                if (lnItem == emptyItemCode && lnBaseItem == baseItemCode)
                                {
                                    emptyVisOrder = r.Columns.First<MatrixRowColumn>(col => col.ID == "0").Value.toInt();
                                    emptyOldQty = Helpers.SAP.ToDouble(r.Columns.First<MatrixRowColumn>(col => col.ID == "11").Value);
                                    emptyWhsCode = r.Columns.First<MatrixRowColumn>(col => col.ID == "24").Value;
                                    break;
                                }
                            }

                            if (emptyVisOrder == 0)
                            {
                                //doesn't exist yet, creat a new one.
                                emptyVisOrder = item38_LinesMatrix.getNewLineVisOrder("1");
                                item38_LinesMatrix.setUIValue(emptyVisOrder, "1", emptyItemCode);
                                item38_LinesMatrix.setUIValue(emptyVisOrder, "24", whsCode); // Assume father's warehouse
                                item38_LinesMatrix.SetCellWithoutValidation(emptyVisOrder, "U_ASC_RTN_ITEM", baseItemCode);
                                item38_LinesMatrix.setUIValue(emptyVisOrder, "11", emptyQty.ToSqlValue());
                                added = true;
                            }
                            else if (emptyOldQty != emptyQty || emptyWhsCode != whsCode)
                            {
                                //Just update the quantity and/or warehouse
                                if (emptyOldQty != emptyQty) item38_LinesMatrix.setUIValue(emptyVisOrder, "11", emptyQty.ToSqlValue());
                                if (emptyWhsCode != whsCode) item38_LinesMatrix.setUIValue(emptyVisOrder, "24", whsCode);
                                added = true;
                            }
                            oRs.MoveNext();
                        }

                        if (added)
                        {
                            // Set focus on the last line
                            // we may have several issues, if for some reason the form lost focus, so we try to make it silently
                            try
                            {
                                int lastRow = item38_LinesMatrix.RowCount;
                                int itemcodeCol = Array.FindIndex(objMatrix.ColumnsInfo, w => w.UniqueID == "1");
                                item38_LinesMatrix.SetCellFocus(lastRow, itemcodeCol);
                            }
                            catch (Exception) { }
                        }
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                    oRs = null;
                    System.GC.Collect();


                }
                if (added)
                    Application.SBO_Application.StatusBar.SetText("Returnable items added/updated.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
                else if (manualUserRequest)
                    Application.SBO_Application.StatusBar.SetText("No returnable items were added or updated.", SAPbouiCOM.BoMessageTime.bmt_Short, SAPbouiCOM.BoStatusBarMessageType.smt_Warning);
            }
            catch (Exception e)
            {
                frmContext.Freeze(false); //make sure the form is unfreezed
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                frmContext.Freeze(false); //make sure the form is unfreezed
            }
            return added; // true if changes occurred
        }
    }
}
