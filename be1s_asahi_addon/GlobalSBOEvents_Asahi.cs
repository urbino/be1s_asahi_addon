﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SAPbouiCOM.Framework;

namespace be1s_asahi_addon
{
    internal class GlobalSBOEvents_Asahi
    {
        static string formsWith_ReturnablesLogic
            /* AR transactions*/ = ",139,140,234234567,180,133,179,"
            /* AP transactions*/ + ",142,143,234234568,182,141,181,";

        static string formsWith_ProcurementORTransporPO
            /* AR Orders + Return Request*/ = ",139,234234567,"
            /* AP Orders + Return Request*/ + ",142,234234568,";

        internal static void FormDataEventHandler(ref SAPbouiCOM.BusinessObjectInfo pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {

                if ((/*DATA ADD*/ pVal.BeforeAction == false && pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_DATA_ADD)
                    && (/*FORMS*/ formsWith_ReturnablesLogic.Contains("," + pVal.FormTypeEx + ","))
                    && (/*SUCCEDED*/pVal.ActionSuccess == true))
                {
                    AddReturnables_AfterFormDataAdd(ref pVal);
                }
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }

        internal static void ItemEventEventHandler(string FormUID, ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if ((/*FORM LOAD AFTER*/ pVal.BeforeAction == false && pVal.EventType == SAPbouiCOM.BoEventTypes.et_FORM_LOAD)
                    && (/*FORM TYPES*/ formsWith_ReturnablesLogic.Contains("," + pVal.FormTypeEx + ",")))
                {
                    AddReturnables_FormLoadAfter(ref pVal);
                } 

                if ((/*ITEM_PRESSED*/ pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                    && (/*FORM TYPES*/ formsWith_ReturnablesLogic.Contains("," + pVal.FormTypeEx + ",")))
                {
                    if (pVal.BeforeAction == true && pVal.ItemUID == "1") GlobalSBOEvents_Asahi.BtOOKAddUpdate_PressedBefore(ref pVal, out BubbleEvent);
                    if (pVal.BeforeAction == false && pVal.ItemUID == "1") GlobalSBOEvents_Asahi.BtOOKAddUpdate_PressedAfter(ref pVal);
                    if (pVal.BeforeAction == false && pVal.ItemUID == "AddRet") GlobalSBOEvents_Asahi.AddReturnables_PressedAfter(ref pVal);
                }

                if ((/*AFTER ITEM_PRESSED*/ pVal.BeforeAction == false && pVal.EventType == SAPbouiCOM.BoEventTypes.et_ITEM_PRESSED)
                    && (/*FORM TYPES*/ formsWith_ProcurementORTransporPO.Contains("," + pVal.FormTypeEx + ","))
                    && (/*ITEM UID*/ pVal.ItemUID == "btPrcrmnt")
                    )
                {
                    GlobalSBOEvents_Asahi.btPrcrmnt_PressedAfter(ref pVal);
                } 
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }

        // variables to hold latest added document ids(we need to catch them in DataEvent to use them in next events in the event flow)
        static int lastDoctype = 0;
        static int lastDocentryAdded = 0;
        internal static void AddReturnables_AfterFormDataAdd(ref SAPbouiCOM.BusinessObjectInfo pVal)
        {   // Catch the new document type and docentry
            lastDoctype = pVal.Type.toInt();
            lastDocentryAdded = Helpers.SAP.DocEntryFromObjectKeyXml(pVal.ObjectKey).toInt();

        }

        private static void AddReturnables_FormLoadAfter(ref SAPbouiCOM.ItemEvent pVal)
        { 
            try
            {
                SAPbouiCOM.Form thisForm = Application.SBO_Application.Forms.Item(pVal.FormUID);
                SAPbouiCOM.Item oItem = (SAPbouiCOM.Item)thisForm.Items.Item("2");  /// Existing Item on the form 

                if (formsWith_ReturnablesLogic.Contains("," + pVal.FormTypeEx + ","))
                {
                    SAPbouiCOM.Item oItem1 = (SAPbouiCOM.Item)thisForm.Items.Add("AddRet", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                    oItem1.Top = oItem.Top;
                    oItem1.Width = oItem.Width + 50;
                    oItem1.Left = oItem.Left + oItem.Width + 10;
                    oItem1.Height = oItem.Height;
                    oItem1.Enabled = true;
                    // oItem1.FromPane = 1; //Make sure the grid is available
                    // oItem1.ToPane = 1; //Make sure the grid is available

                    SAPbouiCOM.Button AddRet = (SAPbouiCOM.Button)oItem1.Specific;
                    AddRet.Caption = "Add returnables";
                    oItem = oItem1;
                }

                //Procurement/transportPO button
                // in. AP+AR Order
                // in. AP+AR Return Request
                if (formsWith_ProcurementORTransporPO.Contains("," + pVal.FormTypeEx + ","))
                {
                    SAPbouiCOM.Item oItem1 = (SAPbouiCOM.Item)thisForm.Items.Add("btPrcrmnt", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                    oItem1.Top = oItem.Top;
                    oItem1.Width = oItem.Width;
                    oItem1.Left = oItem.Left + oItem.Width + 5;
                    oItem1.Height = oItem.Height;
                    oItem1.Enabled = true;

                    SAPbouiCOM.Button btPrcrmnt = (SAPbouiCOM.Button)oItem1.Specific;
                    btPrcrmnt.Caption = "Procurement/Transport";
                    oItem = oItem1;
                }
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }             
        }

        private static void AddReturnables_PressedAfter(ref SAPbouiCOM.ItemEvent pVal)
        {
            try
            {
                Returnables.addORupdateInUI(true);
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }

        private static void btPrcrmnt_PressedAfter(ref SAPbouiCOM.ItemEvent pVal)
        {
            SAPbouiCOM.Form frmContext = SAPbouiCOM.Framework.Application.SBO_Application.Forms.Item(pVal.FormUID);
            try
            {
                frmContext.Freeze(true);

                //GetTable Name from DocNum bindings
                var dtsName = ((SAPbouiCOM.EditText)frmContext.Items.Item("8").Specific).DataBind.TableName;
                var T0 = frmContext.DataSources.DBDataSources.Item(dtsName);
                string ObjType = T0.GetValue("ObjType", 0);
                int DocEntry = T0.GetValue("DocEntry", 0).toInt();

                frmProcurement.LoadIf(ObjType, DocEntry, true);
            }
            catch (Exception e)
            {
                frmContext.Freeze(false); //make sure the form is unfreezed
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                frmContext.Freeze(false); //make sure the form is unfreezed
            }
        }

        private static void BtOOKAddUpdate_PressedBefore(ref SAPbouiCOM.ItemEvent pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                if (Returnables.addORupdateInUI(false)) BubbleEvent = false; //Cancel the event if data was changed      
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }

        private static void BtOOKAddUpdate_PressedAfter(ref SAPbouiCOM.ItemEvent pVal)
        {
            if (lastDocentryAdded != 0)
            {
                try
                {
                    SAPbouiCOM.Form frmContext = SAPbouiCOM.Framework.Application.SBO_Application.Forms.Item(pVal.FormUID);
                    if (pVal.ActionSuccess && frmContext.Mode == SAPbouiCOM.BoFormMode.fm_ADD_MODE)
                    {
                        Application.SBO_Application.OpenForm((SAPbouiCOM.BoFormObjectEnum)lastDoctype, "", lastDocentryAdded.ToString());
                        //Open procurement
                        frmProcurement.LoadIf(lastDoctype.ToString(), lastDocentryAdded, false);
                        frmContext.Close();
                    }
                }
                catch (Exception e)
                {
                    Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
                }

                //make sure values are cleaned
                lastDoctype = 0;
                lastDocentryAdded = 0;
            }
        }
    }
}

