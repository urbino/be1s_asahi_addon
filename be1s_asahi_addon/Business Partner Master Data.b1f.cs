
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace be1s_asahi_addon
{

    //[FormAttribute("134", "Business Partner Master Data.b1f")]
    [FormAttribute("134")] //Don't load UI changes
    class Business_Partner_Master_Data : SystemFormBase
    {
        public Business_Partner_Master_Data()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.txtCardCode = ((SAPbouiCOM.EditText)(this.GetItem("5").Specific)); 
            this.OnCustomInitialize(); 
        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
        }

        private SAPbouiCOM.Button Button2; 
        SAPbouiCOM.Button ASHESTAT;
        private SAPbouiCOM.EditText txtCardCode;
        private void OnCustomInitialize()
        {
            try
            {

                SAPbouiCOM.Item oItem =Button2.Item;

                SAPbouiCOM.Item oItem1 = (SAPbouiCOM.Item)this.UIAPIRawForm.Items.Add("ASHESTAT", SAPbouiCOM.BoFormItemTypes.it_BUTTON);
                oItem1.Top = oItem.Top;
                oItem1.Width = oItem.Width + 50;
                oItem1.Left = oItem.Left + oItem.Width + 10;
                oItem1.Height = oItem.Height;
                oItem1.Enabled = true;

                ASHESTAT = (SAPbouiCOM.Button)oItem1.Specific;
                ASHESTAT.Caption = "Returnable Status";
                ASHESTAT.PressedAfter += new SAPbouiCOM._IButtonEvents_PressedAfterEventHandler(this.ASHESTAT_PressedAfter);

            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }

        private void ASHESTAT_PressedAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {

             try
            {
                RetStatus f = new RetStatus();
                f.loadWithContext(txtCardCode.String);

            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }

 

    }
}
