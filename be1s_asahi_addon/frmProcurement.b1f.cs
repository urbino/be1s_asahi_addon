﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;

namespace be1s_asahi_addon
{
    [FormAttribute("frmProcurement", "frmProcurement.b1f")]
    class frmProcurement : UserFormBase
    {
        public frmProcurement()
        {

        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Button0 = ((SAPbouiCOM.Button)(this.GetItem("btNext").Specific));
            this.Button0.ClickAfter += new SAPbouiCOM._IButtonEvents_ClickAfterEventHandler(this.Button0_ClickAfter);
            this.Button1 = ((SAPbouiCOM.Button)(this.GetItem("2").Specific));
            this.gridTranspSupliers = ((SAPbouiCOM.Grid)(this.GetItem("TRBP").Specific));
            this.gridTranspSupliers.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.gridTranspSupliers_ClickAfter);
            this.gridTransportItems = ((SAPbouiCOM.Grid)(this.GetItem("TRITEM").Specific));
            this.gridTransportItems.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.gridTransportItems_ClickAfter);
            this.CheckBox0 = ((SAPbouiCOM.CheckBox)(this.GetItem("Item_5").Specific));
            this.CheckBox0.PressedAfter += new SAPbouiCOM._ICheckBoxEvents_PressedAfterEventHandler(this.CheckBox0_PressedAfter);
            this.CheckBox0.ClickAfter += new SAPbouiCOM._ICheckBoxEvents_ClickAfterEventHandler(this.CheckBox0_ClickAfter);
            this.gridBreweryBP = ((SAPbouiCOM.Grid)(this.GetItem("BrwBP").Specific));
            this.gridBreweryBP.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.gridBreweryBP_ClickAfter);
            this.gridCollectionAddresses = ((SAPbouiCOM.Grid)(this.GetItem("BrwBPCP").Specific));
            this.gridCollectionAddresses.ClickAfter += new SAPbouiCOM._IGridEvents_ClickAfterEventHandler(this.gridCollectionAddresses_ClickAfter);
            this.EditText0 = ((SAPbouiCOM.EditText)(this.GetItem("Item_8").Specific));
            this.EditText0.ValidateAfter += new SAPbouiCOM._IEditTextEvents_ValidateAfterEventHandler(this.EditText0_ValidateAfter);
            this.EditText0.KeyDownAfter += new SAPbouiCOM._IEditTextEvents_KeyDownAfterEventHandler(this.EditText0_KeyDownAfter);
            this.StaticText8 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_19").Specific));
            this.EditText3 = ((SAPbouiCOM.EditText)(this.GetItem("Item_20").Specific));
            this.EditText3.ValidateAfter += new SAPbouiCOM._IEditTextEvents_ValidateAfterEventHandler(this.EditText3_ValidateAfter);
            this.EditText3.KeyDownAfter += new SAPbouiCOM._IEditTextEvents_KeyDownAfterEventHandler(this.EditText3_KeyDownAfter);
            this.StaticText9 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_21").Specific));
            this.TRADD = ((SAPbouiCOM.CheckBox)(this.GetItem("TRADD").Specific));
            this.TRADD.PressedAfter += new SAPbouiCOM._ICheckBoxEvents_PressedAfterEventHandler(this.TRADD_PressedAfter);
            this.TRNOTES = ((SAPbouiCOM.EditText)(this.GetItem("TRNOTES").Specific));
            this.StaticText11 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_26").Specific));
            this.COLDT = ((SAPbouiCOM.EditText)(this.GetItem("COLDT").Specific));
            this.StaticText14 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_28").Specific));
            this.Folder0 = ((SAPbouiCOM.Folder)(this.GetItem("Item_30").Specific));
            this.Folder1 = ((SAPbouiCOM.Folder)(this.GetItem("Item_31").Specific));
            this.BRNOTES = ((SAPbouiCOM.EditText)(this.GetItem("BRNOTES").Specific));
            this.StaticText15 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_33").Specific));
            this.TRPONUM = ((SAPbouiCOM.EditText)(this.GetItem("TRPONUM").Specific));
            this.StaticText16 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_35").Specific));
            this.BRPONUM = ((SAPbouiCOM.EditText)(this.GetItem("BRPONUM").Specific));
            this.StaticText17 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_38").Specific));
            this.CNAME = ((SAPbouiCOM.EditText)(this.GetItem("CNAME").Specific));
            this.StaticText18 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_40").Specific));
            this.DELADD = ((SAPbouiCOM.EditText)(this.GetItem("DELADD").Specific));
            this.LinkedButton0 = ((SAPbouiCOM.LinkedButton)(this.GetItem("Item_0").Specific));
            this.LinkedButton0.ClickBefore += new SAPbouiCOM._ILinkedButtonEvents_ClickBeforeEventHandler(this.LinkedButton0_ClickBefore);
            this.LinkedButton1 = ((SAPbouiCOM.LinkedButton)(this.GetItem("Item_1").Specific));
            this.LinkedButton1.ClickBefore += new SAPbouiCOM._ILinkedButtonEvents_ClickBeforeEventHandler(this.LinkedButton1_ClickBefore);
            this.SONUM = ((SAPbouiCOM.EditText)(this.GetItem("SONUM").Specific));
            this.LinkedButton2 = ((SAPbouiCOM.LinkedButton)(this.GetItem("Item_3").Specific));
            this.LinkedButton2.ClickBefore += new SAPbouiCOM._ILinkedButtonEvents_ClickBeforeEventHandler(this.LinkedButton2_ClickBefore);
            this.StaticText0 = ((SAPbouiCOM.StaticText)(this.GetItem("Item_4").Specific));
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {
            this.LoadAfter += new LoadAfterHandler(this.Form_LoadAfter);

        }

        private SAPbouiCOM.Button Button0;
        private SAPbouiCOM.Button Button1;
        private SAPbouiCOM.CheckBox CheckBox0;
        private SAPbouiCOM.Grid gridBreweryBP;
        private SAPbouiCOM.Grid gridCollectionAddresses;
        private SAPbouiCOM.Grid gridTranspSupliers;
        private SAPbouiCOM.Grid gridTransportItems;
        private SAPbouiCOM.EditText EditText0;


        string breweryBP = "";
        string breweryBPCollectionAddress = "";
        string transportSupplierCode = "";
        string transportItemCode = "";

        SAPbouiCOM.DataTable MAIN;

        private void OnCustomInitialize()
        {
            this.UIAPIRawForm.DataSources.UserDataSources.Item("UD_0").ValueEx = "1";
            this.UIAPIRawForm.DataSources.UserDataSources.Item("UD_1").ValueEx = "Y";
            this.UIAPIRawForm.DataSources.UserDataSources.Item("UD_2").ValueEx = "Y";


            MAIN = this.UIAPIRawForm.DataSources.DataTables.Item("MAIN");
            MAIN.Rows.Add(1);

            gridBreweryBP.DataTable = this.UIAPIRawForm.DataSources.DataTables.Add("DT0");
            gridCollectionAddresses.DataTable = this.UIAPIRawForm.DataSources.DataTables.Add("DT1");
            gridTranspSupliers.DataTable = this.UIAPIRawForm.DataSources.DataTables.Add("DT2");
            gridTransportItems.DataTable = this.UIAPIRawForm.DataSources.DataTables.Add("DT3");
        }

        private string baseObjectType = "";


        private string request_ObjType = "";
        private int request_DocEntry = 0;
        private string request_NumAtCard = "";

        private string docRequiringTransport_ObjType = "";
        private int docRequiringTransport_DocEntry = 0;
        private string docRequiringTransport_CardCode = "";
        private string docRequiringTransport_CollectionPoint = "";

        private int transportPO_DocEntry = 0;
        private string transportPO_CardCode = "";
        private string transportPO_CollectionPoint = "";
        private string transportPO_ItemCode = "";

        public static void LoadIf(string bsObjType, int bsDocEntry, bool reqByUser)
        {
            try
            {
                if (bsObjType == "17")
                {
                    // procurement from Sales Orders
                    var oRs = (SAPbobsCOM.Recordset)Helpers.SAP.DICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    var cri = "SELECT \"ItemCode\" ";
                    cri += "\n FROM RDR1 ";
                    cri += "\n INNER JOIN OWHS ON rdr1.\"WhsCode\" = OWHS.\"WhsCode\" AND OWHS.\"U_ASH_Procurement\"='Y'";
                    cri += "\n WHERE  \"DocEntry\"=" + bsDocEntry;
                    oRs.DoQuery(cri);
                    if (!oRs.EoF)
                    {
                        frmProcurement f = new frmProcurement();
                        f.load(bsObjType, bsDocEntry);
                    }
                    else
                    {
                        if (reqByUser)
                        {
                            Application.SBO_Application.MessageBox("No lines have a warehouse that uses the procument process.");
                            return;
                        }
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                    oRs = null;
                    System.GC.Collect();
                }
                else if (bsObjType == "22")
                {
                    // Check if the PO is of brewery type
                    var oRs = (SAPbobsCOM.Recordset)Helpers.SAP.DICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    var cri = "SELECT OPOR.\"CardCode\"";
                    cri += "\n FROM OPOR";
                    cri += "\n INNER JOIN OCRD ON OCRD.\"CardCode\" = OPOR.\"CardCode\"";
                    cri += "\n INNER JOIN OCRG ON OCRG.\"GroupCode\" = OCRD.\"GroupCode\" AND OCRG.\"GroupName\" LIKE '%Brewery%'  ";
                    cri += "\n WHERE OPOR.\"DocEntry\"=" + bsDocEntry;
                    oRs.DoQuery(cri);
                    if (oRs.EoF)
                    {
                        if (reqByUser)
                        {
                            Application.SBO_Application.MessageBox("This is not a PO to the Brewery");
                            return;
                        }
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                    oRs = null;
                    System.GC.Collect();

                    // Loaded from Purchase Orders or Return Requests
                    frmProcurement f = new frmProcurement();
                    f.load(bsObjType, bsDocEntry);
                }
                else if (bsObjType == "234000031")
                {
                    // Loaded from AR Return Requests
                    frmProcurement f = new frmProcurement();
                    f.load(bsObjType, bsDocEntry);
                }
                else if (bsObjType == "234000032")
                {
                    // Loaded from AP Return Requests
                    frmProcurement f = new frmProcurement();
                    f.load(bsObjType, bsDocEntry);
                }

            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }

        private bool load(string bsObjType, int bsDocEntry)
        {
            try
            {
                // Cache values
                this.baseObjectType = bsObjType;

                SAPbobsCOM.Recordset oRs;
                string cri;
                if (bsObjType == "17")
                {
                    request_ObjType = bsObjType;
                    request_DocEntry = bsDocEntry;

                    // Find Brewery PO 
                    oRs = (SAPbobsCOM.Recordset)Helpers.SAP.DICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    cri = "SELECT OPOR.\"DocEntry\", OPOR.\"ObjType\"";
                    cri += "\n FROM POR21 ";
                    cri += "\n INNER JOIN OPOR ON OPOR.\"DocEntry\"= POR21.\"DocEntry\" AND OPOR.\"CANCELED\"='N'";
                    cri += "\n INNER JOIN OCRD ON OCRD.\"CardCode\" = OPOR.\"CardCode\"";
                    cri += "\n INNER JOIN OCRG ON OCRG.\"GroupCode\" = OCRD.\"GroupCode\" AND OCRG.\"GroupName\" LIKE '%Brewery%'  ";
                    cri += "\n WHERE POR21.\"ObjectType\" ='22' AND POR21.\"RefDocEntr\"=" + request_DocEntry + " AND POR21.\"RefObjType\" ='" + request_ObjType + "'";
                    oRs.DoQuery(cri);
                    if (!oRs.EoF)
                    {
                        docRequiringTransport_ObjType = (string)oRs.Fields.Item("ObjType").Value;
                        docRequiringTransport_DocEntry = (int)oRs.Fields.Item("DocEntry").Value;
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                    oRs = null;
                    System.GC.Collect();

                }
                else if (bsObjType == "22")  // This is a brewery PO
                {
                    docRequiringTransport_ObjType = bsObjType;
                    docRequiringTransport_DocEntry = bsDocEntry;

                    // Find base Document 
                    oRs = (SAPbobsCOM.Recordset)Helpers.SAP.DICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    cri = "SELECT POR21.\"RefDocEntr\", POR21.\"RefObjType\"";
                    cri += "\n FROM POR21 ";
                    cri += "\n INNER JOIN OPOR ON OPOR.\"DocEntry\"= POR21.\"DocEntry\" AND OPOR.\"CANCELED\"='N'";
                    cri += "\n INNER JOIN OCRD ON OCRD.\"CardCode\" = OPOR.\"CardCode\"";
                    cri += "\n INNER JOIN OCRG ON OCRG.\"GroupCode\" = OCRD.\"GroupCode\" AND OCRG.\"GroupName\" LIKE '%Brewery%'  ";
                    cri += "\n WHERE POR21.\"ObjectType\" ='22' AND POR21.\"DocEntry\"=" + bsDocEntry + " AND POR21.\"RefObjType\" in ('17','234000031','234000032')";
                    oRs.DoQuery(cri);
                    if (!oRs.EoF)
                    {
                        request_ObjType = (string)oRs.Fields.Item("RefObjType").Value;
                        request_DocEntry = (int)oRs.Fields.Item("RefDocEntr").Value;
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                    oRs = null;
                    System.GC.Collect();
                }
                else if (bsObjType == "234000031" || bsObjType == "234000032")
                {
                    docRequiringTransport_ObjType = bsObjType;
                    docRequiringTransport_DocEntry = bsDocEntry;
                }
                else
                {
                    throw new Exception("Base object not expected " + bsObjType);
                }

                // Get data from Sales Order
                if (request_ObjType == "17")
                {
                    oRs = (SAPbobsCOM.Recordset)Helpers.SAP.DICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    cri = "SELECT \"CardName\", \"Address2\", \"DocDueDate\", \"DocNum\", \"NumAtCard\"";
                    cri += "\n FROM ORDR T0";
                    cri += "\n WHERE \"DocEntry\"=" + request_DocEntry;
                    oRs.DoQuery(cri);
                    if (!oRs.EoF)
                    {
                        this.CNAME.String = (string)oRs.Fields.Item("CardName").Value;
                        this.DELADD.String = (string)oRs.Fields.Item("Address2").Value;
                        this.COLDT.Value = ((DateTime)oRs.Fields.Item("DocDueDate").Value).ToSqlValue();

                        request_NumAtCard = (string)oRs.Fields.Item("NumAtCard").Value;
                        this.SONUM.Value = ((int)oRs.Fields.Item("DocNum").Value).ToString();
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                    oRs = null;
                    System.GC.Collect();
                }

                if (docRequiringTransport_ObjType == "22" || docRequiringTransport_ObjType == "234000031" || docRequiringTransport_ObjType == "234000032")
                {
                    // Find Beer/Empties doc that requires transport
                    oRs = (SAPbobsCOM.Recordset)Helpers.SAP.DICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                    cri = "SELECT T0.\"DocEntry\", T0.\"DocNum\", T0.\"CardCode\", T0.\"NumAtCard\",T0.\"Address2\", T0.\"U_ASC_COL_ADD\", T0.\"Comments\", T0.\"DocDueDate\"";
                    if (docRequiringTransport_ObjType == "22") cri += "\n FROM OPOR T0";
                    else if (docRequiringTransport_ObjType == "234000031") cri += "\n FROM ORRR T0";
                    else if (docRequiringTransport_ObjType == "234000032") cri += "\n FROM OPRR T0";
                    else throw new Exception("Object type not expected " + docRequiringTransport_ObjType);
                    cri += "\n WHERE T0.\"DocEntry\" =" + docRequiringTransport_DocEntry;
                    oRs.DoQuery(cri);
                    if (!oRs.EoF)
                    {
                        docRequiringTransport_CardCode = (string)oRs.Fields.Item("CardCode").Value;
                        this.DELADD.String = (string)oRs.Fields.Item("Address2").Value;

                        this.COLDT.Value = ((DateTime)oRs.Fields.Item("DocDueDate").Value).ToSqlValue();
                        docRequiringTransport_CollectionPoint = (string)oRs.Fields.Item("U_ASC_COL_ADD").Value;
                        this.BRPONUM.String = oRs.Fields.Item("DocNum").Value.ToString();
                        this.BRNOTES.String = oRs.Fields.Item("Comments").Value.ToString();
                    }
                    System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                    oRs = null;
                    System.GC.Collect();
                }

                // Find Transport PO
                oRs = (SAPbobsCOM.Recordset)Helpers.SAP.DICompany.GetBusinessObject(SAPbobsCOM.BoObjectTypes.BoRecordset);
                cri = "SELECT T0.\"DocEntry\", T0.\"DocNum\", T0.\"CardCode\", T0.\"Comments\", T0.\"U_ASC_COL_ADD\", T1.\"ItemCode\"";
                cri += "\n FROM OPOR T0";
                cri += "\n INNER JOIN POR1 T1 ON T0.\"DocEntry\"= T1.\"DocEntry\"";
                cri += "\n INNER JOIN POR21 ON T0.\"DocEntry\"= POR21.\"DocEntry\" AND T0.\"CANCELED\"='N'";
                cri += "\n INNER JOIN OCRD ON OCRD.\"CardCode\" = T0.\"CardCode\"";
                cri += "\n INNER JOIN OCRG ON OCRG.\"GroupCode\" = OCRD.\"GroupCode\" AND OCRG.\"GroupName\" LIKE '%Transport%'  ";
                cri += "\n WHERE POR21.\"ObjectType\" ='22' AND POR21.\"RefDocEntr\"=" + docRequiringTransport_DocEntry + " AND POR21.\"RefObjType\" = '" + docRequiringTransport_ObjType + "'";
                oRs.DoQuery(cri);
                if (!oRs.EoF)
                {
                    transportPO_DocEntry = (int)oRs.Fields.Item("DocEntry").Value;
                    transportPO_CardCode = (string)oRs.Fields.Item("CardCode").Value;
                    this.TRPONUM.String = oRs.Fields.Item("DocNum").Value.ToString();
                    this.TRNOTES.String = oRs.Fields.Item("Comments").Value.ToString();

                    transportPO_CollectionPoint = (string)oRs.Fields.Item("U_ASC_COL_ADD").Value;

                    transportPO_ItemCode = (string)oRs.Fields.Item("ItemCode").Value;
                }
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                oRs = null;
                System.GC.Collect();

                // Load brewery BPs
                LoadBreweryBPs();
                LoadCollectionAddresses();

                // Load transport suppliers
                LoadTransportSuppliers();
                LoadTransportItems();

                this.Show();

                setUIState();
                return true;
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
                return false;
            }
        }

        bool createBreweryPO = false;
        bool createTransportPO = false;
        private void setUIState()
        {

            createBreweryPO = false;
            createTransportPO = false;

            Folder1.Item.Enabled = TRADD.Checked;

            if (this.request_ObjType == "17" && this.request_DocEntry != 0)
            {
                // has request (Order, Return request)
                if (docRequiringTransport_DocEntry == 0) createBreweryPO = true;
            }
            if (transportPO_DocEntry == 0 && TRADD.Checked) createTransportPO = true;

            if (createBreweryPO || createTransportPO) this.Button0.Caption = "Add"; else this.Button0.Caption = "Ok";
        }


        private void LoadBreweryBPs()
        {
            try
            {
                this.UIAPIRawForm.Freeze(true);

                string cri = "SELECT DISTINCT  OCRD.\"CardCode\", OCRD.\"CardName\"";
                if (docRequiringTransport_CardCode != "")
                {
                    cri += "\n FROM OCRD";
                    cri += "\n WHERE OCRD.\"CardCode\" = '" + docRequiringTransport_CardCode + "'";
                }
                else
                {
                    if (this.request_ObjType == "17") cri += "\n FROM RDR1 T1";
                    else if (this.request_ObjType == "234000031") cri += "\n FROM RRR1 T1";
                    else if (this.request_ObjType == "234000032") cri += "\n FROM PRR1 T1";
                    else throw new Exception("Base object not expected " + this.request_ObjType);
                    cri += "\n INNER JOIN OITM ON OITM.\"ItemCode\" = T1.\"ItemCode\"";
                    cri += "\n INNER JOIN OCRD ON OCRD.\"CardType\"  = 'S' AND OCRD.\"frozenFor\" = 'N' AND OITM.\"CardCode\" = OCRD.\"CardCode\"";
                    cri += "\n INNER JOIN OCRG ON OCRG.\"GroupName\" LIKE '%Brewery%' AND OCRG.\"GroupCode\" = OCRD.\"GroupCode\" ";
                    cri += "\n INNER JOIN OITB ON OITB.\"ItmsGrpNam\" LIKE 'Beer' AND OITB.\"ItmsGrpCod\" = OITM.\"ItmsGrpCod\"";
                    cri += "\n WHERE t1.\"DocEntry\" = '" + request_DocEntry + "'";
                }
                gridBreweryBP.DataTable.ExecuteQuery(cri);
                breweryBP = "";//clear selection

                gridBreweryBP.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
                for (int i = 0; i < this.gridBreweryBP.Columns.Count; i++)
                {
                    var col = this.gridBreweryBP.Columns.Item(i);
                    col.Editable = false;
                    col.TitleObject.Sortable = true;

                    if (col.UniqueID == "CardCode") col.Visible = false;
                    if (col.UniqueID == "CardName") col.TitleObject.Caption = "Brewery Name";
                }
                gridBreweryBP.AutoResizeColumns();
                autoSelectIfSingleRow(gridBreweryBP);
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                this.UIAPIRawForm.Freeze(false);
            }
        }
        private void LoadCollectionAddresses()
        {
            try
            {
                this.UIAPIRawForm.Freeze(true);
                string cri = " SELECT DISTINCT  CRD1.\"Address\"";
                cri += "\n      , CRD1.\"City\"";
                cri += "\n      ,COALESCE(CRD1.\"Address\",'') ||CHAR(10)||";
                cri += "\n       COALESCE(CRD1.\"Street\",'')  ||CHAR(10)||";
                cri += "\n       COALESCE(CRD1.\"Block\",'')   ||CHAR(10)||";
                cri += "\n       COALESCE(CRD1.\"ZipCode\",'') ||' '||";
                cri += "\n       COALESCE(CRD1.\"City\",'')    ||CHAR(10)||";
                cri += "\n       COALESCE(OCRY.\"Name\",'') AS \"Address2\"";
                cri += "\n FROM CRD1";
                cri += "\n      INNER JOIN OCRD ON  CRD1.\"CardCode\" =  OCRD.\"CardCode\" AND CRD1.\"AdresType\"='S' ";
                cri += "\n         AND OCRD.\"CardCode\" = '" + breweryBP + "'";
                if (docRequiringTransport_CollectionPoint != "")
                {
                    string aaa = docRequiringTransport_CollectionPoint.Split((char)13)[0];
                    cri += "\n        AND CRD1.\"Address\" = '" + aaa + "'";
                }
                cri += "\n      INNER JOIN OCRY ON CRD1.\"Country\" =  OCRY.\"Code\"";
                if (this.request_ObjType == "17" || this.request_ObjType == "234000032")
                {
                    if (this.request_ObjType == "17") cri += "\n INNER JOIN RDR1 T1  ON T1.\"DocEntry\" = '" + request_DocEntry + "'";
                    else if (this.request_ObjType == "234000032") cri += "\n INNER JOIN PRR1 T1 ON T1.\"DocEntry\" = '" + request_DocEntry + "'";
                    else throw new Exception("Base object not expected " + this.request_ObjType);
                    cri += "\n      INNER JOIN OITM ON OITM.\"ItemCode\" = T1.\"ItemCode\"";
                    cri += "\n         AND OITM.\"CardCode\" = OCRD.\"CardCode\"";
                    cri += "\n      INNER JOIN OITB ON OITB.\"ItmsGrpNam\" LIKE 'Beer' AND OITB.\"ItmsGrpCod\" = OITM.\"ItmsGrpCod\"";

                }
                gridCollectionAddresses.DataTable.ExecuteQuery(cri);
                breweryBPCollectionAddress = "";//clear selection

                gridCollectionAddresses.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
                for (int i = 0; i < this.gridCollectionAddresses.Columns.Count; i++)
                {
                    var col = this.gridCollectionAddresses.Columns.Item(i);
                    col.Editable = false;
                    col.TitleObject.Sortable = true;
                    if (col.UniqueID == "Address") col.TitleObject.Caption = "Collection Point";
                    if (col.UniqueID == "Address2") col.Visible = false;
                }

                gridCollectionAddresses.AutoResizeColumns();
                autoSelectIfSingleRow(gridCollectionAddresses);
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                this.UIAPIRawForm.Freeze(false);
            }
        }
        private void LoadTransportSuppliers()
        {
            try
            {
                this.UIAPIRawForm.Freeze(true);

                string cri = "SELECT OCRD.\"CardCode\", OCRD.\"CardName\"";
                cri += "\nFROM OCRD";
                cri += "\nINNER JOIN OCRG ON OCRD.\"GroupCode\" = OCRG.\"GroupCode\" AND OCRG.\"GroupName\" LIKE '%Transport%'";
                if (transportPO_CardCode != "")
                {
                    cri += "\n WHERE OCRD.\"CardCode\"  = '" + transportPO_CardCode + "'";
                }
                else
                {
                    cri += "\n WHERE OCRD.\"CardType\"  = 'S'";
                    cri += "\n   AND OCRD.\"frozenFor\" = 'N'";
                    if (this.CheckBox0.Checked)
                    {
                        cri += "\n   AND EXISTS (SELECT * FROM OSPP";
                        cri += "\n               INNER JOIN OITM ON OITM.\"ItemCode\" = OSPP.\"ItemCode\" and OITM.\"frozenFor\" = 'N'";
                        cri += "\n               INNER JOIN OITB ON OITB.\"ItmsGrpNam\" LIKE '%Transport%' AND OITB.\"ItmsGrpCod\" = OITM.\"ItmsGrpCod\"";
                        cri += "\n               WHERE OSPP.\"CardCode\" = OCRD.\"CardCode\"";
                        cri += "\n                 AND UPPER(OITM.\"ItemName\") LIKE '%" + EditText0.String.ToUpper() + "%'";
                        cri += "\n                 AND UPPER(OITM.\"ItemName\") LIKE '%" + EditText3.String.ToUpper() + "%'";
                        cri += "\n               )";
                    }
                }
                gridTranspSupliers.DataTable.ExecuteQuery(cri);
                transportSupplierCode = "";//clear selection

                gridTranspSupliers.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
                for (int i = 0; i < this.gridTranspSupliers.Columns.Count; i++)
                {
                    var col = this.gridTranspSupliers.Columns.Item(i);
                    col.Editable = false;
                    col.TitleObject.Sortable = true;
                    if (col.UniqueID == "CardCode") col.Visible = false;
                    if (col.UniqueID == "CardName") col.TitleObject.Caption = "Transport Supplier";
                }
                gridTranspSupliers.AutoResizeColumns();
                autoSelectIfSingleRow(gridTranspSupliers);
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                this.UIAPIRawForm.Freeze(false);
            }
        }
        private void LoadTransportItems()
        {
            try
            {
                this.UIAPIRawForm.Freeze(true);

                string cri = "SELECT OITM.\"ItemCode\", OITM.\"ItemName\"";
                cri += "\n  FROM OITM ";
                cri += "\n INNER JOIN OITB ON OITB.\"ItmsGrpNam\" LIKE '%Transport%' AND OITB.\"ItmsGrpCod\" = OITM.\"ItmsGrpCod\"";
                if (transportPO_ItemCode != "")
                {
                    cri += "\n WHERE OITM.\"ItemCode\" = '" + transportPO_ItemCode + "'";
                }
                else
                {
                    cri += "\n WHERE OITM.\"frozenFor\" = 'N'";
                    cri += "\n   AND UPPER(OITM.\"ItemName\") LIKE '%" + EditText0.String.ToUpper() + "%'";
                    cri += "\n   AND UPPER(OITM.\"ItemName\") LIKE '%" + EditText3.String.ToUpper() + "%'";
                    if (this.CheckBox0.Checked)
                    {
                        cri += "\n   AND EXISTS (SELECT * FROM OSPP ";
                        cri += "\n               WHERE OITM.\"ItemCode\" = OSPP.\"ItemCode\"";
                        cri += "\n                 AND OSPP.\"CardCode\"='" + transportSupplierCode + "')";
                    }
                }
                gridTransportItems.DataTable.ExecuteQuery(cri);
                transportItemCode = "";//clear selection

                gridTransportItems.SelectionMode = SAPbouiCOM.BoMatrixSelect.ms_Single;
                for (int i = 0; i < this.gridTransportItems.Columns.Count; i++)
                {
                    var col = this.gridTransportItems.Columns.Item(i);
                    col.Editable = false;
                    col.TitleObject.Sortable = true;
                    if (col.UniqueID == "ItemCode") col.Visible = false;
                    if (col.UniqueID == "itemName") col.TitleObject.Caption = "Lane";
                }
                gridTransportItems.AutoResizeColumns();
                autoSelectIfSingleRow(gridTransportItems);
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                this.UIAPIRawForm.Freeze(false);
            }
        }


        private void autoSelectIfSingleRow(SAPbouiCOM.Grid ogrid)
        {
            if (ogrid.DataTable.Rows.Count == 1) selectRow(ogrid, 0);
        }

        private void selectRow(SAPbouiCOM.Grid ogrid, int row)
        {
            try
            {
                this.UIAPIRawForm.Freeze(true);
                if (row < 0) return;

                ogrid.Rows.SelectedRows.Clear();
                ogrid.Rows.SelectedRows.Add(row);
                int rowIX = ogrid.GetDataTableRowIndex(row);

                if (ogrid == gridBreweryBP)
                {
                    breweryBP = ogrid.DataTable.dt_get_string("CardCode", rowIX);
                    LoadCollectionAddresses();
                }
                if (ogrid == gridCollectionAddresses)
                {
                    breweryBPCollectionAddress = ogrid.DataTable.dt_get_string("Address2", rowIX);

                    EditText0.Value = ogrid.DataTable.dt_get_string("City", rowIX) + "_to";
                    LoadTransportSuppliers();
                }
                if (ogrid == gridTranspSupliers)
                {
                    transportSupplierCode = ogrid.DataTable.dt_get_string("CardCode", rowIX);
                    LoadTransportItems();
                }
                if (ogrid == gridTransportItems)
                {
                    transportItemCode = ogrid.DataTable.dt_get_string("ItemCode", rowIX);
                }
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                this.UIAPIRawForm.Freeze(false);
            }
        }



        private void CheckBox0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {

        }

        private void CheckBox0_PressedAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            LoadTransportSuppliers();
            LoadTransportItems();
        }


        bool clicking = false;
        private void Button0_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (clicking) return; //don't allow multiple clicks
            try
            {
                clicking = true;


                if (!createBreweryPO && !createTransportPO)
                {
                    this.UIAPIRawForm.Close();
                    return;
                }

                //if (createBreweryPO)
                // {
                if (breweryBP == "")
                {
                    Application.SBO_Application.StatusBar.SetText("Please, select a brewery.", SAPbouiCOM.BoMessageTime.bmt_Short);
                    return;
                }

                if (breweryBPCollectionAddress == "")
                {
                    Application.SBO_Application.StatusBar.SetText("Please, select a collection point address.", SAPbouiCOM.BoMessageTime.bmt_Short);
                    return;
                }
                //  }

                if (createTransportPO)
                {
                    if (transportSupplierCode == "")
                    {
                        Application.SBO_Application.StatusBar.SetText("Please, select a transport vendor.", SAPbouiCOM.BoMessageTime.bmt_Short);
                        return;
                    }

                    if (transportItemCode == "")
                    {
                        Application.SBO_Application.StatusBar.SetText("Please, select a transport Item.", SAPbouiCOM.BoMessageTime.bmt_Short);
                        return;
                    }
                }

                // Add procurement Document 
                if (createBreweryPO)
                {
                    string jsonData = Helpers.SAP.ServiceLayer.Get("/Orders(" + request_DocEntry + ")");
                    dynamic SO = Newtonsoft.Json.Linq.JObject.Parse(jsonData);

                    dynamic doc = new System.Dynamic.ExpandoObject();
                    doc.DocType = "dDocument_Items";
                    doc.CardCode = breweryBP;
                    doc.DocDueDate = this.COLDT.Value.ToString();
                    doc.Address2 = this.DELADD.String;
                    doc.NumAtCard = request_NumAtCard;
                    doc.U_ASC_COL_ADD = breweryBPCollectionAddress;
                    doc.Comments = this.BRNOTES.Value;

                    var lines = new List<dynamic>();
                    foreach (var soline in SO.DocumentLines)
                    {
                        var line = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
                        foreach (var field in soline)
                        {
                            if (field.Name.StartsWith("U_")
                                || field.Name == "ItemCode"
                                || field.Name == "Quantity"
                                || field.Name == "UoMCode"
                                || field.Name == "CostingCode"
                                || field.Name == "CostingCode2"
                                || field.Name == "CostingCode3"
                                || field.Name == "CostingCode4"
                                || field.Name == "CostingCode5")
                            {
                                line.Add(field.Name, field.Value);
                            }
                        }
                        lines.Add(line);
                    }
                    doc.DocumentLines = lines;

                    var dRefs = new List<dynamic>();
                    dRefs.Add(new { RefDocEntr = request_DocEntry, RefObjType = request_ObjType });
                    doc.DocumentReferences = dRefs;

                    jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.Indented);
                    jsonData = Helpers.SAP.ServiceLayer.Post("/PurchaseOrders", jsonData);
                    dynamic beerPO = Newtonsoft.Json.Linq.JObject.Parse(jsonData);

                    //Data for Transport PO
                    docRequiringTransport_DocEntry = beerPO.DocEntry;
                    docRequiringTransport_ObjType = "22";

                    Application.SBO_Application.OpenForm(SAPbouiCOM.BoFormObjectEnum.fo_PurchaseOrder, "", beerPO.DocEntry);
                }


                // Add transport Document
                if (createTransportPO)
                {
                    string jsonData;
                    if (docRequiringTransport_ObjType == "22") jsonData = Helpers.SAP.ServiceLayer.Get("/PurchaseOrders(" + docRequiringTransport_DocEntry + ")");
                    else if (docRequiringTransport_ObjType == "234000031") jsonData = Helpers.SAP.ServiceLayer.Get("/ReturnRequest(" + docRequiringTransport_DocEntry + ")");
                    else if (docRequiringTransport_ObjType == "234000032") jsonData = Helpers.SAP.ServiceLayer.Get("/GoodsReturnRequest(" + docRequiringTransport_DocEntry + ")");
                    else throw new Exception("Object type not expected " + docRequiringTransport_ObjType);

                    dynamic bsDOC = Newtonsoft.Json.Linq.JObject.Parse(jsonData);

                    dynamic doc2 = new System.Dynamic.ExpandoObject();
                    doc2.DocType = "dDocument_Items";
                    doc2.CardCode = transportSupplierCode;
                    doc2.NumAtCard = request_NumAtCard;
                    doc2.DocDueDate = this.COLDT.Value.ToString();

                    doc2.Address2 = this.DELADD.String;
                    doc2.U_ASC_COL_ADD = breweryBPCollectionAddress;
                    doc2.Comments = this.TRNOTES.Value;

                    var lines2 = new List<dynamic>();
                    var line2 = new System.Dynamic.ExpandoObject() as IDictionary<string, Object>;
                    line2.Add("ItemCode", transportItemCode);
                    line2.Add("Quantity", 1);
                    //Assume CostCenters
                    var soline = bsDOC.DocumentLines[0];
                    foreach (var field in soline)
                    {
                        if (  field.Name == "CostingCode"
                            || field.Name == "CostingCode2"
                            || field.Name == "CostingCode3"
                            || field.Name == "CostingCode4"
                            || field.Name == "CostingCode5")
                        {
                            line2.Add(field.Name, field.Value);
                        }
                    }
                    lines2.Add(line2);
                    doc2.DocumentLines = lines2;

                    var dRefs2 = new List<dynamic>();
                    if (request_DocEntry != 0) dRefs2.Add(new { RefDocEntr = request_DocEntry, RefObjType = request_ObjType });
                    if (docRequiringTransport_DocEntry != 0) dRefs2.Add(new { RefDocEntr = docRequiringTransport_DocEntry, RefObjType = docRequiringTransport_ObjType });
                    doc2.DocumentReferences = dRefs2;

                    jsonData = Newtonsoft.Json.JsonConvert.SerializeObject(doc2, Newtonsoft.Json.Formatting.Indented);
                    jsonData = Helpers.SAP.ServiceLayer.Post("/PurchaseOrders", jsonData);
                    dynamic transportPO = Newtonsoft.Json.Linq.JObject.Parse(jsonData);

                    Application.SBO_Application.OpenForm(SAPbouiCOM.BoFormObjectEnum.fo_PurchaseOrder, "", transportPO.DocEntry);
                }
                this.UIAPIRawForm.Close();

            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                clicking = false;
            }
        }

        private void gridBreweryBP_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {

            selectRow(gridBreweryBP, pVal.Row);
            try
            {
                this.UIAPIRawForm.Freeze(true);
                if (pVal.Row < 0) return;
                gridBreweryBP.Rows.SelectedRows.Clear();
                if (pVal.Row >= 0) gridBreweryBP.Rows.SelectedRows.Add(pVal.Row);
                breweryBP = gridBreweryBP.DataTable.dt_get_string("CardCode", gridBreweryBP.GetDataTableRowIndex(pVal.Row));


                LoadCollectionAddresses();
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                this.UIAPIRawForm.Freeze(false);
            }

        }

        private void gridCollectionAddresses_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            selectRow(gridCollectionAddresses, pVal.Row);
        }

        private void gridTranspSupliers_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            selectRow(gridTranspSupliers, pVal.Row);
        }

        private void gridTransportItems_ClickAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            selectRow(gridTransportItems, pVal.Row);
        }

        private SAPbouiCOM.StaticText StaticText8;
        private SAPbouiCOM.EditText EditText3;
        private SAPbouiCOM.StaticText StaticText9;

        private void EditText0_KeyDownAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
        }

        private void EditText3_KeyDownAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
        }

        private SAPbouiCOM.CheckBox TRADD;
        private SAPbouiCOM.EditText TRNOTES;
        private SAPbouiCOM.StaticText StaticText11;
        private SAPbouiCOM.EditText COLDT;
        private SAPbouiCOM.StaticText StaticText14;
        private SAPbouiCOM.Folder Folder0;
        private SAPbouiCOM.Folder Folder1;
        private SAPbouiCOM.EditText BRNOTES;
        private SAPbouiCOM.StaticText StaticText15;
        private SAPbouiCOM.EditText TRPONUM;
        private SAPbouiCOM.StaticText StaticText16;
        private SAPbouiCOM.EditText BRPONUM;
        private SAPbouiCOM.StaticText StaticText17;
        private SAPbouiCOM.EditText CNAME;
        private SAPbouiCOM.StaticText StaticText18;
        private SAPbouiCOM.EditText DELADD;


        private void TRADD_PressedAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            Folder0.Select();

            setUIState();
        }

        private void Form_LoadAfter(SAPbouiCOM.SBOItemEventArg pVal)
        {

        }

        private void EditText3_ValidateAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            if (pVal.ItemChanged) LoadTransportSuppliers();

        }

        private void EditText0_ValidateAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {

            if (pVal.ItemChanged) LoadTransportSuppliers();
        }


        private SAPbouiCOM.LinkedButton LinkedButton0;


        private void LinkedButton0_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = false;

            Application.SBO_Application.OpenForm((SAPbouiCOM.BoFormObjectEnum)this.docRequiringTransport_ObjType.toInt(), "", this.docRequiringTransport_DocEntry.ToString());
        }

        private SAPbouiCOM.LinkedButton LinkedButton1;

        private void LinkedButton1_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {

            BubbleEvent = false;
            Application.SBO_Application.OpenForm(SAPbouiCOM.BoFormObjectEnum.fo_PurchaseOrder, "", transportPO_DocEntry.ToString());
        }

        private SAPbouiCOM.EditText SONUM;
        private SAPbouiCOM.LinkedButton LinkedButton2;
        private SAPbouiCOM.StaticText StaticText0;

        private void LinkedButton2_ClickBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = false;

            Application.SBO_Application.OpenForm((SAPbouiCOM.BoFormObjectEnum)this.request_ObjType.toInt(), "", this.request_DocEntry.ToString());
        }
    }
}
