﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;
using be1s_asahi_addon;

namespace be1s_asahi_addon
{
    [FormAttribute("RetStatusDet", "RetStatusDet.b1f")]
    class RetStatusDet : UserFormBase
    {
        public RetStatusDet()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Grid1 = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.Grid1.LinkPressedBefore += new SAPbouiCOM._IGridEvents_LinkPressedBeforeEventHandler(this.Grid1_LinkPressedBefore);
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("btOk").Specific));
            this.Button2.PressedAfter += new SAPbouiCOM._IButtonEvents_PressedAfterEventHandler(this.Button2_PressedAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {

        }

        private SAPbouiCOM.Grid Grid1;
        private SAPbouiCOM.DataTable DT1;
        private SAPbouiCOM.DataTable DT2;
        private SAPbouiCOM.DataTable queryDT;
        private SAPbouiCOM.Button Button2;
        private SAPbouiCOM.UserDataSource FOLDER;

        private void OnCustomInitialize()
        {


            this.FOLDER = ((SAPbouiCOM.UserDataSource)(this.UIAPIRawForm.DataSources.UserDataSources.Item("FOLDER")));
            this.FOLDER.ValueEx = "1"; //To activate folder 1

            DT1 = UIAPIRawForm.DataSources.DataTables.Add("DT1");

            Grid1.DataTable = DT1;
        }


        string BPCode = "";
        string Itemcode = "";
        int Year = 0;
        int Month = 0;
        public void loadWithContext(string BPCode, string Itemcode, int Year, int Month)
        {
            try
            {

                UIAPIRawForm.Freeze(true);

                this.BPCode = BPCode;
                this.Itemcode = Itemcode;
                this.Year = Year;
                this.Month = Month;
                fillSearch2();

                this.Show();
            }
            finally
            {
                UIAPIRawForm.Freeze(false);
            }
        }



        private void fillSearch2()
        {
            try
            {
                UIAPIRawForm.Freeze(true);
                string sql = "SELECT  RS.\"RowNumber\"";
                sql += "\n , RS.\"DocDate\"";
                sql += "\n , RS.\"DocTypeName\"   AS \"Document\"";
                sql += "\n , RS.\"DocNum\"        AS \"Doc Number\"";  
                sql += "\n , RS.\"NumAtCard\"  AS \"BP Ref. Num.\""; 
                sql += "\n , RS.\"Quantity\"";
                sql += "\n , RS.\"QtyBalToDate\" AS \"Qty Balance\"";
                sql += "\n , RS.\"PriceBefDi\" AS \"Price\"";
                sql += "\n , RS.\"LineTotal\"";
                sql += "\n , RS.\"SumBalToDate\" AS \"Sum Balance\"";
                sql += "\n , RS.\"Currency\"";
                sql += "\n , RS.\"BaseDocTypeName\"  AS \"Base Document\"";
                sql += "\n , RS.\"BaseRef\"          AS \"Base Doc Num\""; 
                sql += "\n , RS.\"ItmsGrpNam\" AS \"Group\"";
                sql += "\n , RS.\"ItemCode\"";
                sql += "\n , RS.\"ItemName\"";
                sql += "\n , RS.\"CardCode\"";
                sql += "\n , RS.\"CardName\"";
                sql += "\n , RS.\"DocType\" ";
                sql += "\n , RS.\"DocEntry\" ";
                sql += "\n FROM \"ASH_B1S_RETURNABLES\" AS RS";
                sql += "\n INNER JOIN \"_SYS_BI\".M_TIME_DIMENSION MTD ON RS.\"DocDate\" = MTD.DATE_SQL";
                sql += "\n WHERE RS.\"CardCode\"='" + this.BPCode + "'";
                sql += "\n   AND RS.\"ItemCode\"='" + this.Itemcode + "'";
                if (this.Year != 0) sql += "\n   and MTD.\"YEAR\"   = " + this.Year;
                if (this.Month != 0) sql += "\n   and MTD.\"MONTH\"  = " + this.Month;
                sql += "\n ORDER By RS.\"RowNumber\"";

                Grid1.DataTable.ExecuteQuery(sql);  

                for (int i = 0; i < this.Grid1.Columns.Count; i++)
                {
                    var col = this.Grid1.Columns.Item(i);
                    if (col.UniqueID == "Price") col.RightJustified = true;
                    if (col.UniqueID == "Qty Balance") col.RightJustified = true;
                    if (col.UniqueID == "LineTotal") col.RightJustified = true;
                    if (col.UniqueID == "Sum Balance") col.RightJustified = true;

                    if (col.UniqueID == "Doc Number")
                    { 
                        ((SAPbouiCOM.EditTextColumn)col).LinkedObjectType = "13";//really doesn't matter is just to show the arrow
                    }
                    
                    if (col.UniqueID == "DocType") col.Visible = false;
                    if (col.UniqueID == "DocEntry") col.Visible = false; 
                    col.Editable = false;
                }
                Grid1.AutoResizeColumns();


                this.Show();
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                UIAPIRawForm.Freeze(false);
            }
        }
        private void Grid1_LinkPressedBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                //Set the correct document type for the link

                var col = (SAPbouiCOM.EditTextColumn)this.Grid1.Columns.Item(pVal.ColUID);
                int dtRowIx = Grid1.GetDataTableRowIndex(pVal.Row);
                BubbleEvent = false;

                int ObjType  = DT1.dt_get_string("DocType", dtRowIx).toInt();
                int DocEntry = (int)DT1.dt_get_value("DocEntry", dtRowIx);
                Application.SBO_Application.OpenForm((SAPbouiCOM.BoFormObjectEnum)ObjType, "", DocEntry.ToString());
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }


        /*
        private void Grid2_LinkPressedBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                //Set the correct document type for the link

                var col = (SAPbouiCOM.EditTextColumn)this.Grid2.Columns.Item(pVal.ColUID);
                int dtRowIx = Grid2.GetDataTableRowIndex(pVal.Row);
                BubbleEvent = false;
                if (pVal.ColUID == "201") ((SAPbouiCOM.EditTextColumn)this.Grid2.Columns.Item("BatchAbsEntry")).PressLink(pVal.Row);
                else if (pVal.ColUID == "301") ((SAPbouiCOM.EditTextColumn)this.Grid2.Columns.Item("BinAbsEntry")).PressLink(pVal.Row);
                else
                {
                    BubbleEvent = true;
                }

            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }
         * */


        private void Button2_PressedAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Close();
        }



    }
}
