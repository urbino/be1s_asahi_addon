﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace be1s_asahi_addon
{
    [XmlRoot(ElementName = "Column")]
    public class MemoryDatatableColumn
    {
        [XmlAttribute(AttributeName = "Uid")]
        public string Uid { get; set; }

        [XmlAttribute(AttributeName = "Type")]
        public string Type { get; set; }

        [XmlAttribute(AttributeName = "MaxLength")]
        public string MaxLength { get; set; }
    }

    [XmlRoot(ElementName = "Columns")]
    public class MemoryDatatableColumns
    {
        [XmlElement(ElementName = "Column")]
        public List<MemoryDatatableColumn> ColumnList { get; set; }
    }

    [XmlRoot(ElementName = "Cell")]
    public class MemoryDatatableCell
    {
        [XmlElement(ElementName = "ColumnUid")]
        public string ColumnUid { get; set; }

        [XmlElement(ElementName = "Value")]
        public string Value { get; set; }
    }

    [XmlRoot(ElementName = "Cells")]
    public class MemoryDatatableCells
    {
        [XmlElement(ElementName = "Cell")]
        public List<MemoryDatatableCell> CellList { get; set; }
    }

    [XmlRoot(ElementName = "Row")]
    public class MemoryDataTableRow
    {
        [XmlElement(ElementName = "Cells")]
        public MemoryDatatableCells Cells { get; set; }
    }

    [XmlRoot(ElementName = "Rows")]
    public class MemoryDatatableRows
    {
        [XmlElement(ElementName = "Row")]
        public List<MemoryDataTableRow> RowList { get; set; }
    }

    [XmlRoot(ElementName = "DataTable")]
    public class MemoryDataTable
    {
        [XmlElement(ElementName = "Columns")]
        public MemoryDatatableColumns Columns { get; set; }

        [XmlElement(ElementName = "Rows")]
        public MemoryDatatableRows Rows { get; set; }

        [XmlAttribute(AttributeName = "Uid")]
        public string Uid { get; set; }


        public int colIndex(string column)
        {
            for (int i = 0; i < Columns.ColumnList.Count; i++)
            {
                if (Columns.ColumnList[i].Uid == column) return i;
            }
            throw new Exception("Column not found " + column);
        }
        
        public int GetMaxValue(string column)
        {
            int max = -99999999;

            for (int i = 0; i < Rows.RowList.Count; i++)
            {
                MemoryDataTableRow r = Rows.RowList[i];
                for (int j = 0; j < Rows.RowList[i].Cells.CellList.Count; j++)
                {
                    MemoryDatatableCell c = Rows.RowList[i].Cells.CellList[j];
                    if (c.ColumnUid == column)
                    {
                        int val = int.Parse("0" + c.Value);
                        if (val > max) max = val;
                    }
                }
            }
            return max;
        }
         
    }

}
