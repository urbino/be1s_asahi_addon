﻿using System;
using System.Collections.Generic;
using SAPbouiCOM.Framework;

namespace be1s_asahi_addon
{
    class Program
    {
         
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                Application oApp = null;
                if (args.Length < 1)
                {
                    oApp = new Application();
                }
                else
                {
                    oApp = new Application(args[0]);
                }

                // Connect DiAPI; 
                Helpers.SAP.ConnectDI();

                // Connect ServiceLayer;
                Helpers.SAP.ServiceLayer.Connect();
                 
                Menu MyMenu = new Menu();
                MyMenu.AddMenuItems();
                oApp.RegisterMenuEventHandler(MyMenu.SBO_Application_MenuEvent);

                GlobalSBOEvents.RegisterGlobalEvents();

                Application.SBO_Application.StatusBar.SetText(Helpers.AssemblyInfo.Title+" connected and running.", SAPbouiCOM.BoMessageTime.bmt_Medium, SAPbouiCOM.BoStatusBarMessageType.smt_Success);
                 
                 oApp.Run();
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message);
            }
        }
    }
}
