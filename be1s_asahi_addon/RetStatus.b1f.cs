﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SAPbouiCOM.Framework;
using be1s_asahi_addon;

namespace be1s_asahi_addon
{
    [FormAttribute("RetStatus", "RetStatus.b1f")]
    class RetStatus : UserFormBase
    {
        public RetStatus()
        {
        }

        /// <summary>
        /// Initialize components. Called by framework after form created.
        /// </summary>
        public override void OnInitializeComponent()
        {
            this.Grid1 = ((SAPbouiCOM.Grid)(this.GetItem("Item_0").Specific));
            this.Grid1.LinkPressedBefore += new SAPbouiCOM._IGridEvents_LinkPressedBeforeEventHandler(this.Grid1_LinkPressedBefore);
            this.Button2 = ((SAPbouiCOM.Button)(this.GetItem("btOk").Specific));
            this.Button2.PressedAfter += new SAPbouiCOM._IButtonEvents_PressedAfterEventHandler(this.Button2_PressedAfter);
            this.OnCustomInitialize();

        }

        /// <summary>
        /// Initialize form event. Called by framework before form creation.
        /// </summary>
        public override void OnInitializeFormEvents()
        {

        }

        private SAPbouiCOM.Grid Grid1;
        private SAPbouiCOM.DataTable DT1;
        private SAPbouiCOM.Button Button2;
        private SAPbouiCOM.UserDataSource FOLDER;

        private void OnCustomInitialize()
        {


            this.FOLDER = ((SAPbouiCOM.UserDataSource)(this.UIAPIRawForm.DataSources.UserDataSources.Item("FOLDER")));
            this.FOLDER.ValueEx = "1"; //To activate folder 1

            DT1 = UIAPIRawForm.DataSources.DataTables.Add("DT1");

            Grid1.DataTable = DT1;
        }

        string BPCode = "";
        public void loadWithContext(string BPCode)
        {
            try
            {

                UIAPIRawForm.Freeze(true);

                this.BPCode = BPCode;
                fillSearch();

                this.Show();
            }
            finally
            {
                UIAPIRawForm.Freeze(false);
            }
        }

        private void fillSearch()
        {
            try
            {
                UIAPIRawForm.Freeze(true);
                string sql = "CALL ASH_B1S_GET_RETURNABLES_BPSTATUS('" + this.BPCode + "')";
                Grid1.DataTable.ExecuteQuery(sql);
                Grid1.CollapseLevel = 2;

                for (int i = 0; i < this.Grid1.Columns.Count; i++)
                {
                    var col = this.Grid1.Columns.Item(i);
                    if (col.UniqueID == "Group") { }
                    else if (col.UniqueID == "Description") { }
                    else if (col.UniqueID == "ItemCode")
                    {
                        ((SAPbouiCOM.EditTextColumn)col).LinkedObjectType = "4";// items, realyy doesn't matter is just to show the arrow
                    }
                    else if (col.UniqueID == "Year")
                    {
                        ((SAPbouiCOM.EditTextColumn)col).LinkedObjectType = "4";// items, realyy doesn't matter is just to show the arrow
                    }
                    else if (col.UniqueID == "Month")
                    {
                        ((SAPbouiCOM.EditTextColumn)col).LinkedObjectType = "4";// items, realyy doesn't matter is just to show the arrow
                    }
                    else if (col.UniqueID == "DocEntry")
                    {
                        col.TitleObject.Caption = "Internal Key";
                        ((SAPbouiCOM.EditTextColumn)col).LinkedObjectType = "4";// items, realyy doesn't matter is just to show the arrow
                    }
                    else
                    {
                        col.RightJustified = true;
                    }
                    col.Editable = false;
                }
                Grid1.AutoResizeColumns();


                this.Show();
            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
            finally
            {
                UIAPIRawForm.Freeze(false);
            }
        }

        private void Grid1_LinkPressedBefore(object sboObject, SAPbouiCOM.SBOItemEventArg pVal, out bool BubbleEvent)
        {
            BubbleEvent = true;
            try
            {
                //Set the correct document type for the link

                var col = (SAPbouiCOM.EditTextColumn)this.Grid1.Columns.Item(pVal.ColUID);
                int dtRowIx = Grid1.GetDataTableRowIndex(pVal.Row);
                BubbleEvent = false; //Cancel standard event

                string ItemCode = DT1.dt_get_string("ItemCode", dtRowIx);
                int Year = 0;
                int Month = 0;

                if (pVal.ColUID == "Year" || pVal.ColUID == "Month") Year = DT1.dt_get_string("Year", dtRowIx).toInt();
                if (pVal.ColUID == "Month") Month = DT1.dt_get_string("Month", dtRowIx).toInt();

                RetStatusDet f = new RetStatusDet();
                f.loadWithContext(this.BPCode, ItemCode, Year, Month);


            }
            catch (Exception e)
            {
                Application.SBO_Application.MessageBox("Error:" + e.Message + "\n" + e.ToString());
            }
        }
        private void Button2_PressedAfter(object sboObject, SAPbouiCOM.SBOItemEventArg pVal)
        {
            this.UIAPIRawForm.Close();
        }
    }
}
