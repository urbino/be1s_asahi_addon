﻿using SAPbobsCOM;
using SAPbouiCOM.Framework;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace be1s_asahi_addon
{
    class Helpers
    {
        private static string Get_OADM_FieldValue(string fieldName)
        {
            string val = null;

            Recordset oRs = (Recordset)Helpers.SAP.DICompany.GetBusinessObject(BoObjectTypes.BoRecordset);
            try
            {
                string cri = "";
                cri = @"select """ + fieldName + @""" from OADM";
                oRs.DoQuery(cri);
                if (!oRs.EoF)
                {
                    val = oRs.Fields.Item(0).Value.ToString();
                }
                else
                {
                    val = "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(oRs);
                oRs = null;
                System.GC.Collect();
            }

            return val;
        }


        public static class FileSystem
        {
            private class windowHandleInfo : System.Windows.Forms.IWin32Window
            {
                public IntPtr Handle { get; set; }
            }


            private static windowHandleInfo getSBOWindowHandle()
            {
                foreach (System.Diagnostics.Process pList in System.Diagnostics.Process.GetProcesses())
                {
                    if (pList.MainWindowTitle.Equals(Application.SBO_Application.Desktop.Title))
                    {
                        IntPtr hWnd = pList.MainWindowHandle;
                        windowHandleInfo w = new windowHandleInfo() { Handle = hWnd };

                        return w;
                    }
                }

                throw new Exception("Not possile to find SBO window handle");
            }

            private static string PickDirectory(bool modal)
            {
                string PATH = "";

                // Create a new, STA thread, invoke it 
                System.Threading.Thread thread1 = new System.Threading.Thread(() =>
                {
                    System.Windows.Forms.FolderBrowserDialog dialog = new System.Windows.Forms.FolderBrowserDialog();
                    dialog.RootFolder = Environment.SpecialFolder.MyComputer;

                    if (modal)
                        dialog.ShowDialog(getSBOWindowHandle());
                    else
                        dialog.ShowDialog();

                    PATH = dialog.SelectedPath;

                });
                thread1.SetApartmentState(System.Threading.ApartmentState.STA);
                thread1.Start();
                thread1.Join();//Esperar pela execução 

                return PATH;
            }

            private static string PickFile(bool modal, string filter = "All files (*.*)|*.*")
            {
                string PATH = "";
                // Create a new, STA thread, invoke it 
                System.Threading.Thread thread1 = new System.Threading.Thread(() =>
                {
                    System.Windows.Forms.OpenFileDialog dialog = new System.Windows.Forms.OpenFileDialog();
                    dialog.InitialDirectory = Environment.SpecialFolder.MyComputer.ToString();
                    dialog.Filter = filter;
                    if (modal)
                        dialog.ShowDialog(getSBOWindowHandle());
                    else
                        dialog.ShowDialog();
                    PATH = dialog.FileName;
                });

                thread1.SetApartmentState(System.Threading.ApartmentState.STA);
                thread1.Start();
                thread1.Join();//Esperar pela execução

                return PATH;
            }

            public static string GetNextAvailableFilename(string fullname)
            {
                if (!System.IO.File.Exists(fullname)) return fullname;


                string folder = System.IO.Path.GetDirectoryName(fullname);
                string plainName = System.IO.Path.GetFileNameWithoutExtension(fullname);
                string extension = System.IO.Path.GetExtension(fullname);
                string alternateFilename;
                int fileNameIndex = 0;
                do
                {
                    fileNameIndex += 1;
                    alternateFilename = string.Format("{0}{1}{2}", System.IO.Path.Combine(folder, plainName), fileNameIndex, extension);

                } while (System.IO.File.Exists(alternateFilename));

                return alternateFilename;
            }

            private static System.IO.FileInfo Copy_to_B1(string fileLocation, string b1_location)
            {
                if (!string.IsNullOrWhiteSpace(fileLocation))
                {
                    System.IO.FileInfo oFile = new System.IO.FileInfo(fileLocation);
                    var name = oFile.Name;

                    string sapb1Fname = GetNextAvailableFilename(b1_location + name);

                    if (System.IO.File.Exists(b1_location + name))
                    {
                        var namesemext = oFile.Name.Replace(oFile.Extension, "");
                        name = namesemext + DateTime.Now.ToString("dd_MM_yyyy_HH_mm") + oFile.Extension;
                    }

                    oFile.CopyTo(sapb1Fname);
                    oFile = new System.IO.FileInfo(sapb1Fname);

                    return oFile;
                }
                else
                {
                    return null;
                }
            }

            public static System.IO.FileInfo Search_Attachment_and_Copy_to_B1(bool modal, string fileLocation = "")
            {
                if (fileLocation == "") fileLocation = PickFile(modal);

                return Copy_to_B1(fileLocation, Helpers.SAP.DICompany.AttachMentPath);
            }

            public static System.IO.FileInfo Search_Picture_and_Copy_to_B1(bool modal, string fileLocation = "")
            {
                string filter = "Graphic files|*.bmp;*.jpg;*.jpeg;*.png;*.gif;*.tif;*.pcx;*.tga;*.bif|All files (*.*)|*.*";
                if (fileLocation == "") fileLocation = PickFile(modal, filter);

                System.IO.FileInfo resultFile = Copy_to_B1(fileLocation, Helpers.SAP.DICompany.BitMapPath);
                return resultFile;
            }
        }

        internal static class AssemblyInfo
        {
            public static string Company { get { return GetExecutingAssemblyAttribute<System.Reflection.AssemblyCompanyAttribute>(a => a.Company); } }
            public static string Product { get { return GetExecutingAssemblyAttribute<System.Reflection.AssemblyProductAttribute>(a => a.Product); } }
            public static string Copyright { get { return GetExecutingAssemblyAttribute<System.Reflection.AssemblyCopyrightAttribute>(a => a.Copyright); } }
            public static string Trademark { get { return GetExecutingAssemblyAttribute<System.Reflection.AssemblyTrademarkAttribute>(a => a.Trademark); } }
            public static string Title { get { return GetExecutingAssemblyAttribute<System.Reflection.AssemblyTitleAttribute>(a => a.Title); } }
            public static string Description { get { return GetExecutingAssemblyAttribute<System.Reflection.AssemblyDescriptionAttribute>(a => a.Description); } }
            public static string Configuration { get { return GetExecutingAssemblyAttribute<System.Reflection.AssemblyDescriptionAttribute>(a => a.Description); } }
            public static string FileVersion { get { return GetExecutingAssemblyAttribute<System.Reflection.AssemblyFileVersionAttribute>(a => a.Version); } }

            public static Version Version { get { return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version; } }
            public static string VersionFull { get { return Version.ToString(); } }
            public static string VersionMajor { get { return Version.Major.ToString(); } }
            public static string VersionMinor { get { return Version.Minor.ToString(); } }
            public static string VersionBuild { get { return Version.Build.ToString(); } }
            public static string VersionRevision { get { return Version.Revision.ToString(); } }

            private static string GetExecutingAssemblyAttribute<T>(Func<T, string> value) where T : Attribute
            {
                T attribute = (T)Attribute.GetCustomAttribute(System.Reflection.Assembly.GetExecutingAssembly(), typeof(T));
                return value.Invoke(attribute);
            }
        }

        internal static class SAP
        {

            private static SAPbobsCOM.Company _Company = null;
            public static SAPbobsCOM.Company DICompany
            {
                get
                {
                    return _Company;
                }
            }
            public static void ConnectDI()
            {
                try
                {
                    _Company = (SAPbobsCOM.Company)Application.SBO_Application.Company.GetDICompany();
                }
                catch (System.Exception ex)
                {
                    // DO NOTHING
                    _Company = null;
                    throw new Exception("Get service layer connection context error: " + ex.Message, ex);
                }
            }
            public static class Cache
            {
                static string _DecimalSeparator = null;
                static string _ThousandSeparator = null;
                static string _DateSeparator = null;


                static int? _SumDec = null;
                static int? _PriceDec = null;
                static int? _PercentDec = null;

                public static string DecimalSeparator
                {
                    get
                    {
                        if (_DecimalSeparator == null) _DecimalSeparator = Get_OADM_FieldValue("DecSep");
                        return _DecimalSeparator;
                    }
                }

                public static string ThousandSeparator
                {
                    get
                    {
                        if (_ThousandSeparator == null) _ThousandSeparator = Get_OADM_FieldValue("ThousSep");
                        return _ThousandSeparator;
                    }
                }

                public static string DateSeparator
                {
                    get
                    {
                        if (_DateSeparator == null) _DateSeparator = Get_OADM_FieldValue("DateSep");
                        return _DateSeparator;
                    }
                }

                public static int SumDec
                {
                    get
                    {
                        if (_SumDec == null) _SumDec = int.Parse(Get_OADM_FieldValue("SumDec"));
                        return (int)_SumDec;
                    }
                }

                public static int PriceDec
                {
                    get
                    {
                        if (_PriceDec == null) _PriceDec = int.Parse(Get_OADM_FieldValue("PriceDec"));
                        return (int)_PriceDec;
                    }
                }

                public static int PercentDec
                {
                    get
                    {
                        if (_PercentDec == null) _PercentDec = int.Parse(Get_OADM_FieldValue("PercentDec"));
                        return (int)_PercentDec;
                    }
                }


            }



            internal static class ServiceLayer
            {

                static string _ServiceLayerUrl = null;
                static string sConnectionContext = null;

                private static string ServiceLayerUrl
                {
                    get
                    {
                        if (_ServiceLayerUrl == null)
                        {
                            string serverNameOrIP = Application.SBO_Application.Company.ServerName.Split(':')[0];
                            _ServiceLayerUrl = "https://" + serverNameOrIP + ":50000/b1s/v1";
                        }
                        return _ServiceLayerUrl;
                    }
                }


                public static void Connect()
                {
                    try
                    {
                        sConnectionContext = Application.SBO_Application.Company.GetServiceLayerConnectionContext(ServiceLayerUrl);
                    }
                    catch (System.Exception ex)
                    {
                        // DO NOTHING
                        sConnectionContext = null;
                        throw new Exception("Get service layer connection context error: " + ex.Message, ex);
                    }
                }

                public static string Get(string slUri)
                {
                    return Execute("GET", slUri, "");
                }
                public static string Post(string slUri, string payload)
                {
                    return Execute("POST", slUri, payload);
                }

                public static string Execute(string httpMethod, string slUri, string payload)
                {
                    try
                    {
                        if (sConnectionContext == null) ServiceLayer.Connect();

                        int attempts = 0;
                        string responseContent = null;
                        do
                        {
                            var request = System.Net.WebRequest.Create(ServiceLayerUrl + slUri) as System.Net.HttpWebRequest;
                            request.Method = httpMethod;

                            request.AllowAutoRedirect = false;
                            request.Timeout = 30 * 1000;
                            request.ServicePoint.Expect100Continue = false;
                            request.CookieContainer = new System.Net.CookieContainer();
                            request.ServerCertificateValidationCallback += (sender, cert, chain, error) =>
                            {
                                return true; // Accept self signed HTTP/s (and everything else....)
                            };

                            // set cookies
                            string[] cookieItems = sConnectionContext.Split(';');
                            foreach (var cookieItem in cookieItems)
                            {
                                string[] parts = cookieItem.Split('=');
                                if (parts.Length == 2)
                                {
                                    request.CookieContainer.Add(request.RequestUri, new System.Net.Cookie(parts[0].Trim(), parts[1].Trim()));
                                }
                            }

                            if (payload != "")
                            {
                                //Send body
                                var data = Encoding.ASCII.GetBytes(payload.Replace('\'', '"'));
                                request.ContentLength = data.Length;
                                using (var stream = request.GetRequestStream())
                                {
                                    stream.Write(data, 0, data.Length);
                                }
                            }


                            try
                            {
                                using (System.Net.HttpWebResponse response = request.GetResponse() as System.Net.HttpWebResponse)
                                {
                                    if (response.StatusCode != System.Net.HttpStatusCode.OK && response.StatusCode != System.Net.HttpStatusCode.Created)
                                    {
                                        throw new Exception("SL " + httpMethod + " error " + response.StatusDescription);
                                    }

                                    // Read the response
                                    using (var reader = new System.IO.StreamReader(response.GetResponseStream(), Encoding.UTF8))
                                    {
                                        responseContent = reader.ReadToEnd();
                                    }
                                    break;
                                }
                            }
                            catch (System.Net.WebException e)
                            {
                                string errContent = "";
                                string errMsg = e.Message;

                                try
                                {
                                    using (System.Net.WebResponse response = e.Response)
                                    {
                                        System.Net.HttpWebResponse httpResponse = (System.Net.HttpWebResponse)response;
                                        if (httpResponse.StatusCode == System.Net.HttpStatusCode.Unauthorized && attempts == 0)
                                        {
                                            // the authentication has probably expired. lets reconnect once.
                                            attempts++;
                                            Connect();
                                            continue;
                                        }
                                        Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                                        using (System.IO.Stream data = response.GetResponseStream())
                                        using (var reader = new System.IO.StreamReader(data))
                                        {
                                            errContent = reader.ReadToEnd();
                                        }
                                        dynamic ret = Newtonsoft.Json.Linq.JObject.Parse(errContent);
                                        errMsg += "\n\n" + ret.error.message.value;
                                    }
                                }
                                catch (Exception ex) { }


                                try
                                {
                                    System.IO.File.WriteAllText(System.IO.Path.GetTempPath() + "be1s_asahi_addon_SLcall.Err"
, @"CALL: 
" + httpMethod + " " + slUri + @"

PAYLOAD:
" + payload + @"

RETURN CONTENT:
" + errContent + @"

ERR_MESSAGE:
" + errMsg + @"

STACK_TRACE:
" + e.StackTrace.ToString()
);
                                }
                                catch (Exception) { }

                                throw new Exception(errMsg, e);
                            }
                            catch (System.Exception ex)
                            {
                                throw new Exception("callServiceLayer err:" + ex.Message, ex);
                            }

                        } while (attempts <= 1);


                        // dynamic result = Newtonsoft.Json.Linq.JObject.Parse(responseContent);
                        return responseContent;

                    }
                    catch (System.Exception ex)
                    {
                        throw new Exception("callServiceLayer err:" + ex.Message, ex);
                    }
                }
            }

            public static string DocEntryFromObjectKeyXml(string objkey)
            {
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.LoadXml(objkey);
                return xmlDoc.SelectSingleNode("//DocEntry").InnerText;
            }
            public static string AbsoluteEntryFromObjectKeyXml(string objkey)
            {
                System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
                xmlDoc.LoadXml(objkey);
                return xmlDoc.SelectSingleNode("//AbsoluteEntry").InnerText;
            }
            public static double Round_ToSum(double Sum)
            {
                return Math.Round(Sum, Cache.SumDec);
            }

            public static double Round_ToPrice(double Price)
            {
                return Math.Round(Price, Cache.PriceDec);
            }

            public static double Round_ToPercent(double Rate)
            {
                return Math.Round(Rate, Cache.PercentDec);
            }

            public static double ToDouble(string Prop_Str)
            {
                string valor = Prop_Str;

                if (string.IsNullOrEmpty(valor)) return 0;

                if (valor.IndexOf(' ') > 0)
                    valor = valor.Left(valor.IndexOf(' ')); //remover moeda,  exemplo: EUR

                valor = valor.Replace(Cache.ThousandSeparator, "");

                if (string.IsNullOrEmpty(Prop_Str))
                    return 0;

                double val = double.Parse(valor.Replace(Cache.DecimalSeparator, System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));

                return val;
            }

            public static double ToDouble(SAPbouiCOM.EditText oItem)
            {
                return ToDouble(oItem.String);
            }

            public static double ToDouble(SAPbouiCOM.Cell cell)
            {
                return ToDouble((SAPbouiCOM.EditText)cell.Specific);

            }

            public static DateTime ToDateTime(SAPbouiCOM.Cell cell)
            {
                bool sucesso = false;
                DateTime data = DateTime.MinValue;

                if (((SAPbouiCOM.EditText)cell.Specific).Value != "")
                    sucesso = DateTime.TryParseExact(((SAPbouiCOM.EditText)cell.Specific).Value, "yyyyMMdd",
                        CultureInfo.InvariantCulture, DateTimeStyles.None, out data);

                return data;

            }

            public static DateTime ToDateTime(string val)
            {
                bool sucesso = false;
                DateTime data = DateTime.MinValue;

                if (val != "")
                    sucesso = DateTime.TryParseExact(val, "yyyyMMdd",
                        CultureInfo.InvariantCulture, DateTimeStyles.None, out data);

                return data;

            }

            public static DateTime ToDateTime(SAPbouiCOM.Cell cell, string DateFormat)
            {
                bool sucesso = false;

                if (DateFormat == "")
                {
                    DateFormat = "yyyyMMdd";
                }

                DateTime data = DateTime.MinValue;

                if (((SAPbouiCOM.EditText)cell.Specific).Value != "")
                    sucesso = DateTime.TryParseExact(((SAPbouiCOM.EditText)cell.Specific).Value, DateFormat,
                        CultureInfo.InvariantCulture, DateTimeStyles.None, out data);

                return data;

            }

            public static DateTime ToDateTime(string DateStr, string DateFormat)
            {
                bool sucesso = false;

                if (DateFormat == "")
                {
                    DateFormat = "yyyyMMdd";
                }

                DateTime data = DateTime.MinValue;

                if (DateStr != "")
                    sucesso = DateTime.TryParseExact(DateStr, DateFormat,
                        CultureInfo.InvariantCulture, DateTimeStyles.None, out data);

                return data;

            }

            public static string ToStringFromSum(double val)
            {
                return val.ToString("0." + new string('0', Cache.SumDec)).Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, Cache.DecimalSeparator);
            }

            public static string ToStringFromPrice(double val)
            {
                return val.ToString("0." + new string('0', Cache.PriceDec)).Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, Cache.DecimalSeparator);
            }


            public static string ToStringFromPercent(double val)
            {
                return val.ToString("0." + new string('0', Cache.PercentDec)).Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, Cache.DecimalSeparator);
            }

            public static string ToStringSAP(double val)
            {
                string valor = val.ToString();
                valor = valor.Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, Cache.DecimalSeparator);

                return valor;
            }
            public static string ToStringSAPWithThousands(double val)
            {
                string valor = val.ToString("N");
                valor = valor.Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyGroupSeparator, "|");
                valor = valor.Replace(CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator, Cache.DecimalSeparator);
                valor = valor.Replace("|", Cache.ThousandSeparator);
                valor = valor.Replace(Cache.DecimalSeparator + new string('0', Cache.PriceDec), "");
                valor = valor.Replace(Cache.DecimalSeparator + new string('0', Cache.SumDec), "");
                valor = valor.Replace(Cache.DecimalSeparator + new string('0', Cache.PercentDec), "");
                return valor;
            }

            public static double toDoubleMilimeters(string mesureString)
            {
                bool cm = false;
                bool dm = false;
                bool m = false;

                string val = "";
                if (mesureString.Contains("mm")) { val = mesureString.ToLower().Replace("mm", ""); }
                else if (mesureString.Contains("cm")) { val = mesureString.ToLower().Replace("cm", ""); cm = true; }
                else if (mesureString.Contains("dm")) { val = mesureString.ToLower().Replace("dm", ""); dm = true; }
                else if (mesureString.Contains("m")) { val = mesureString.ToLower().Replace("m", ""); m = true; }
                else { val = mesureString.ToLower(); }

                double value = Helpers.SAP.ToDouble(val);
                {
                    if (cm) value = value * 10;
                    if (dm) value = value * 100;
                    if (m) value = value * 1000;
                }

                return value;
            }

            public static string formatMeasure(string mesureString)
            {
                bool mm = false;
                bool cm = false;
                bool dm = false;
                bool m = false;

                string val = "";
                if (mesureString.Contains("mm")) { val = mesureString.ToLower().Replace("mm", ""); }
                else if (mesureString.Contains("cm")) { val = mesureString.ToLower().Replace("cm", ""); cm = true; }
                else if (mesureString.Contains("dm")) { val = mesureString.ToLower().Replace("dm", ""); dm = true; }
                else if (mesureString.Contains("m")) { val = mesureString.ToLower().Replace("m", ""); m = true; }
                else { val = mesureString.ToLower(); mm = true; }
                if (val == "") return "";

                double value = Helpers.SAP.ToDouble(val);
                {
                    if (mm) return ToStringSAPWithThousands(value) + "mm";
                    if (cm) return ToStringSAPWithThousands(value) + "cm";
                    if (dm) return ToStringSAPWithThousands(value) + "dm";
                    if (m) return ToStringSAPWithThousands(value) + "m";
                }

                return ToStringSAPWithThousands(value) + "mm";
            }

            public static string formatArea(string mesureString)
            {
                bool mm2 = false;
                bool cm2 = false;
                bool dm2 = false;
                bool m2 = false;

                string val = "";
                if (mesureString.Contains("mm")) { val = mesureString.ToLower().Replace("mm2", "").Replace("mm", ""); mm2 = true; }
                else if (mesureString.Contains("cm")) { val = mesureString.ToLower().Replace("cm2", "").Replace("cm", ""); cm2 = true; }
                else if (mesureString.Contains("dm")) { val = mesureString.ToLower().Replace("dm2", "").Replace("dm", ""); dm2 = true; }
                else if (mesureString.Contains("m")) { val = mesureString.ToLower().Replace("m2", "").Replace("m", ""); m2 = true; }
                else { val = mesureString.ToLower(); mm2 = true; }
                if (val == "") return "";
                double value = Helpers.SAP.ToDouble(val);
                {
                    if (mm2) return ToStringSAPWithThousands(value) + "mm2";
                    if (cm2) return ToStringSAPWithThousands(value) + "cm2";
                    if (dm2) return ToStringSAPWithThousands(value) + "dm2";
                    if (m2) return ToStringSAPWithThousands(value) + "m2";
                }

                return ToStringSAPWithThousands(value) + "mm2";
            }
            public static double toDoubleAreaMm2(string mesureString)
            {
                bool mm2 = false;
                bool cm2 = false;
                bool dm2 = false;
                bool m2 = false;

                string val = "";
                if (mesureString.Contains("mm")) { val = mesureString.ToLower().Replace("mm2", "").Replace("mm", ""); mm2 = true; }
                else if (mesureString.Contains("cm")) { val = mesureString.ToLower().Replace("cm2", "").Replace("cm", ""); cm2 = true; }
                else if (mesureString.Contains("dm")) { val = mesureString.ToLower().Replace("dm2", "").Replace("dm", ""); dm2 = true; }
                else if (mesureString.Contains("m")) { val = mesureString.ToLower().Replace("m2", "").Replace("m", ""); m2 = true; }
                else { val = mesureString.ToLower(); mm2 = true; }

                double value = Helpers.SAP.ToDouble(val);
                {
                    if (mm2) value = value * 1;
                    if (cm2) value = value * 100;
                    if (dm2) value = value * 10000;
                    if (m2) value = value * 1000000;
                }

                return value;
            }

            public static double toDoubleAreaM2(string mesureString)
            {
                return toDoubleAreaMm2(mesureString) / 1000000;
            }
        }

        public static class Image
        {
            public static System.IO.FileInfo CreateQuotationThumbnail(string stPhotoPath)
            {
                return ResizeImage(300, 200, stPhotoPath);
            }

            public static System.IO.FileInfo ResizeImage(int newWidth, int newHeight, string stPhotoPath)
            {
                System.Drawing.Image imgPhoto = System.Drawing.Image.FromFile(stPhotoPath);

                int sourceWidth = imgPhoto.Width;
                int sourceHeight = imgPhoto.Height;

                //Consider vertical pics
                if (sourceWidth < sourceHeight)
                {
                    int buff = newWidth;

                    newWidth = newHeight;
                    newHeight = buff;
                }

                int sourceX = 0, sourceY = 0, destX = 0, destY = 0;
                float nPercent = 0, nPercentW = 0, nPercentH = 0;

                nPercentW = ((float)newWidth / (float)sourceWidth);
                nPercentH = ((float)newHeight / (float)sourceHeight);
                if (nPercentH < nPercentW)
                {
                    nPercent = nPercentH;
                    destX = System.Convert.ToInt16((newWidth - (sourceWidth * nPercent)) / 2);
                }
                else
                {
                    nPercent = nPercentW;
                    destY = 0;// System.Convert.ToInt16((newHeight - (sourceHeight * nPercent)) / 2);
                }

                int destWidth = (int)(sourceWidth * nPercent);
                int destHeight = (int)(sourceHeight * nPercent);


                System.Drawing.Bitmap bmPhoto = new System.Drawing.Bitmap(newWidth, newHeight, System.Drawing.Imaging.PixelFormat.Format24bppRgb);

                bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

                System.Drawing.Graphics grPhoto = System.Drawing.Graphics.FromImage(bmPhoto);
                grPhoto.Clear(System.Drawing.Color.White);
                grPhoto.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;

                grPhoto.DrawImage(imgPhoto,
                    new System.Drawing.Rectangle(destX, destY, destWidth, destHeight),
                    new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                    System.Drawing.GraphicsUnit.Pixel);


                string folder = System.IO.Path.GetDirectoryName(stPhotoPath);
                string plainName = System.IO.Path.GetFileNameWithoutExtension(stPhotoPath);
                string tempFname = System.IO.Path.Combine(folder, plainName + ".jpg");
                string newFname = Helpers.FileSystem.GetNextAvailableFilename(tempFname);
                bmPhoto.Save(newFname, System.Drawing.Imaging.ImageFormat.Jpeg);
                grPhoto.Dispose();
                imgPhoto.Dispose();
                bmPhoto.Dispose();
                return new System.IO.FileInfo(newFname);
            }

        }
    }
}
