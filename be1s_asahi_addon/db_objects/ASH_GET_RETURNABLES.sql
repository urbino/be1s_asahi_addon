﻿DROP PROCEDURE ASH_GET_RETURNABLES;

CREATE PROCEDURE ASH_GET_RETURNABLES ( 
	in itemcode 	nvarchar(30), 		  
	in quantity 	decimal(21,6),
	out ret 		ASH_RETURNABLES_TT
)
LANGUAGE SQLSCRIPT 
SQL SECURITY INVOKER
READS SQL DATA
AS 
BEGIN 
	ret= SELECT COALESCE(T0."U_ASC_RtnKegCode",'') AS ITEMCODE
			, cast(:quantity as decimal(21,6)) as QUANTITY
			, T0."ItemCode"  AS BASEITEM																									
			FROM OITM T0 
			INNER JOIN OITM retItem  on retItem."ItemCode" =  T0."U_ASC_RtnKegCode"  
			WHERE T0."ItemCode" 		= :itemcode 
			  AND T0."U_ASC_RtnKegCode" IS NOT NULL  
			  
	UNION ALL -- Crates
		SELECT COALESCE(T0."U_ASC_RtnCraCode",'') AS "ItemCode"		
			, cast(
				   CASE WHEN T0."U_ASC_RtnCraQty"      <> 0 AND T0."U_ASC_Pack"='DOLLY' -- Special case for DOLLY, when the RntCraQty is used inverselly
				  		THEN  :quantity * T0."U_ASC_RtnCraQty" 
		          		ELSE :quantity END 
          			as decimal(21,6)) as QUANTITY	
			, T0."ItemCode" AS BASEITEM
		    FROM OITM T0 
			INNER JOIN OITM retItem  on retItem."ItemCode" =  T0."U_ASC_RtnCraCode"  
		    WHERE T0."ItemCode" = :itemcode 
		      AND T0."U_ASC_RtnCraCode" IS NOT NULL
		      AND T0."U_ASC_RtnCraQty" <> 0
		      
	UNION ALL -- Bottles 
		SELECT COALESCE(T0."U_ASC_RtnBolCode",'') AS "ItemCode"
			, CAST( 
					:quantity * T0."U_ASC_RtnBolQty" 
          			as decimal(21,6)) as QUANTITY  	
			, T0."ItemCode" AS BASEITEM														
			FROM OITM T0 
			INNER JOIN OITM retItem  on retItem."ItemCode" =  T0."U_ASC_RtnBolCode"  
			WHERE T0."ItemCode" 		= :itemcode 
			  AND T0."U_ASC_RtnBolCode" IS NOT NULL
			  AND  T0."U_ASC_RtnBolQty" <> 0
			  		      
	UNION ALL -- Palletes (of kegs/Cases)
		SELECT COALESCE(T0."U_ASC_RtnPalCode",'') AS "ItemCode"
			, cast( CASE WHEN T0."U_ASC_RtnKegQty"      <> 0 THEN CEIL(ROUND( :quantity / T0."U_ASC_RtnKegQty",1))  
						 WHEN T0."U_ASC_CasePerPallet"  <> 0 THEN CEIL(ROUND( :quantity / T0."U_ASC_CasePerPallet",1)) 
						 WHEN T0."U_ASC_RtnCraQty"      <> 0 THEN CEIL(ROUND( :quantity / T0."U_ASC_RtnCraQty",1)) 
					     ELSE 0.0 
					     END as DECIMAL(21,6)) as QUANTITY	
			, T0."ItemCode" AS BASEITEM
			FROM OITM T0 
			INNER JOIN OITM retItem  on retItem."ItemCode" =  T0."U_ASC_RtnPalCode"  
			WHERE T0."ItemCode" 		= :itemcode
			  AND T0."U_ASC_RtnPalCode" IS NOT NULL 
			  AND ( T0."U_ASC_RtnKegQty"  <> 0  OR  T0."U_ASC_CasePerPallet" <> 0 OR T0."U_ASC_RtnCraQty" <> 0) 
	   ;
END ;