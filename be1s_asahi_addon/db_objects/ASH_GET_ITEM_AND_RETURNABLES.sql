﻿CREATE PROCEDURE ASH_GET_ITEM_AND_RETURNABLES (
	in customergln 	nvarchar(255),
	in itemcode 	nvarchar(30), 		 
	in substitute 	nvarchar(30),  	 
	in itemgln 		nvarchar(30),  
	in quantity 	decimal(21,6), 
	in uom 			nvarchar(30)
)
LANGUAGE SQLSCRIPT 
SQL SECURITY INVOKER
READS SQL DATA
AS
BEGIN
	 DECLARE returnables BE1S_ASAHI_RETURNABLES_TT;
  	-- Code to find the item
  	item = SELECT "ItemCode" AS ITEMCODE, :quantity AS QUANTITY
  		   FROM OITM
  		   WHERE "ItemCode" = :itemcode;
  	
  	-- Call BE1S_ASAHI_GET_RETURNABLES to get the returnables
  	CALL BE1S_ASAHI_GET_RETURNABLES(:itemcode,:quantity,:returnables);
  	
  	
  	hasReturnables = SELECT BASEITEM, count(*) AS NR from :returnables group by BASEITEM;
  	
  	-- RETURN THE RESULTS
  	SELECT item.ITEMCODE, item.QUANTITY
  	, CASE WHEN COALESCE(hasReturnables.NR,0)=0 THEN 'N' ELSE 'Y' END AS RTN_ADDED
  	, CASE WHEN COALESCE(hasReturnables.NR,0)=0 THEN '' ELSE item.ITEMCODE END AS RTN_ITEM
  	FROM :item item
  	LEFT join :hasReturnables hasReturnables ON ITEM.ITEMCODE = hasReturnables.BASEITEM
  	UNION ALL
  	SELECT ret.ITEMCODE, ret.QUANTITY, 'N' AS RTN_ADDED , ret.BASEITEM AS RTN_ITEM
  	FROM :returnables ret;
  	
END